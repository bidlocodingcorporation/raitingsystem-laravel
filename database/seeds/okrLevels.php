<?php

use Illuminate\Database\Seeder;

class okrLevels extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $okrLevelBachelor = new \App\okr();
        $okrLevelBachelor->okrLevel = 'Бакалавр';
        $okrLevelBachelor->save();

        $okrLevelMaster = new \App\okr();
        $okrLevelMaster->okrLevel = 'Магiстр';
        $okrLevelMaster->save();
    }
}
