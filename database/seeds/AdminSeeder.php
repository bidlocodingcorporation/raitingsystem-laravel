<?php

use App\User;
use App\faculty;
use App\field;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Никита';
        $user->surname = 'Третынко';
        $user->secondName = 'Андреевич';
        $user->faculty_id = faculty::find(1)->id;
        $user->field_id = field::find(1)->id;
        $user->role_id = role::find(1)->id;
        $user->group = '1234';
        $user->email = 'tretynko.nikita.95@gmail.com';
        $user->password = Hash::make('12345678');
        $user->studentPhone = '+380997765865';
        $user->save();
    }
}
