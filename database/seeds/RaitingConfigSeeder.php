<?php

use Illuminate\Database\Seeder;
use App\raitingConfig;

class RaitingConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ratingConfig = raitingConfig::find(1);
        $ratingConfig->responsiblePersonName = 'Кондратенко Світлана Вікторівна';
        $ratingConfig->responsiblePersonTelephone = '228-76-48';
        $ratingConfig->save();
    }
}
