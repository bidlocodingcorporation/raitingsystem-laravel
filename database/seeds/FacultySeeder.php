<?php

use Illuminate\Database\Seeder;
use App\faculty;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faculty = new faculty();
        $faculty->facultyName = 'Математический';
        $faculty->save();
    }
}
