<?php

use Illuminate\Database\Seeder;
use App\field;

class FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $field = new field();
        $field->fieldName = 'Программная инженерия';
        $field->save();
    }
}
