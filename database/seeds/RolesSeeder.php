<?php

use Illuminate\Database\Seeder;
use App\role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = new role();
        $adminRole->roleName = 'admin';
        $adminRole->save();

        $studentRole = new role();
        $studentRole->roleName = 'student';
        $studentRole->save();
    }
}
