<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminLoggingToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_level_of_awards', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')
                ->nullable();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('document_type_level_of_awards', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')
                ->nullable();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('document_type_of_awards', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')
                ->nullable();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('document_type_of_partisipations', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')
                ->nullable();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('faculties', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')
                ->nullable();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('fields', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')
                ->nullable();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('okrs', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')
                ->nullable();

            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_level_of_awards', function (Blueprint $table) {
            $table->dropForeign('document_level_of_awards_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::table('document_type_level_of_awards', function (Blueprint $table) {
            $table->dropForeign('document_type_level_of_awards_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::table('document_type_of_awards', function (Blueprint $table) {
            $table->dropForeign('document_type_of_awards_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::table('document_type_of_partisipations', function (Blueprint $table) {
            $table->dropForeign('document_type_of_partisipations_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::table('faculties', function (Blueprint $table) {
            $table->dropForeign('faculties_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::table('fields', function (Blueprint $table) {
            $table->dropForeign('fields_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::table('okrs', function (Blueprint $table) {
            $table->dropForeign('okrs_id_foreign');
            $table->dropColumn('user_id');
        });
    }
}
