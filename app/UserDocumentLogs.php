<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDocumentLogs extends Model
{
    public const SENDED_STATUS = 1;
    public const EMAIL_SENDED_MESSAGE = 'Вiдправлено на електронну адресу';
    public const EMAIL_SENDING_FAILED = 'Не вiдправлено';
    protected $fillable = ['status', 'user_id'];

    public function getStatus()
    {
        if ($this->status == self::SENDED_STATUS) {
            return self::EMAIL_SENDED_MESSAGE;
        }

        return self::EMAIL_SENDING_FAILED;
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
