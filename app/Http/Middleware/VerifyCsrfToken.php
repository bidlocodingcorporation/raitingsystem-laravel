<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/admin/manageFaculties/save',
        '/admin/manageFields/save',
        '/admin/manageDocumentLevelOfAwards/store',
        '/admin/manageDocumentTypeOfAwards/store',
        '/admin/manageDocumentTypeOfPartisipation/store',
        '/student/createDocument/store',
        '/admin/manageUsers/changeRole',
        '/admin/manageraitingconfig/store',
        '/admin/managePointsCalc/store',
        '/admin/manageOkr/store',
        '/student/profile/updateProfile/store',
        '/admin/manageUsers/importUsers/import',
        '/admin/manageDocumentTypeOfAwards/importTypeOfAwards',
        '/admin/manageDocumentLevelOfAwards/import',
        '/admin/manageDocumentTypeOfPartisipation/import',
        '/admin/manageFields/import',
        '/admin/manageFaculties/import',
        '/admin/managePointsCalc/import',
        '/admin/manageOkr/import',
        '/admin/manageraitingconfig/import',
    ];
}
