<?php

namespace App\Http\Controllers;

use App\Exports\FieldsExport;
use App\faculty;
use App\Imports\FieldsImport;
use App\okr;
use Illuminate\Http\Request;
use App\field;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class FieldController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, array(
            'fieldName' => 'required',
            'faculty' => 'required',
            'okr' => 'required',
        ));

        $id = $request->post('field_id');
        if ($id) {
            $field = field::find($id);
            $this->setField($field, $request);

            return redirect('/admin/manageFields')->with('success', __('Освiтня програма додана'));
        }

        if ($request->post('fieldName')) {
            $field = new field();
            $this->setField($field, $request);

            return redirect('/admin/manageFields')->with('success', __('Освiтня програма додана'));
        }

        return redirect()->back()->with('error', __('Сталася помилка'));
    }

    public function remove($id)
    {
        if ($id) {
            field::find($id)->delete();

            return redirect()->back()->with('success', __('Освiтня програма успiшно видалено'));
        }

        return redirect()->back()->with('error', __('Сталася помилка'));
    }

    public function getFieldsByFaculty($id)
    {
        $faculty['data'] = faculty::find($id)->getFields();
        echo json_encode($faculty);
        exit;
    }

    public function getFilterByOkr($id)
    {
        $okr['data'] = okr::find($id)->getFields();
        echo json_encode($okr);
        exit;
    }

    public function import(Request $request)
    {
        $this->validate($request, array(
           'importFile' => 'required'
        ));

        Excel::import(new FieldsImport(), $request->file('importFile'));

        return redirect()->back()->with('success', __('admin.importTypeOfAwardsSuccess'));
    }

    public function export()
    {
        return Excel::download(new FieldsExport(), 'fields.xlsx');
    }

    private function setField($field, $request)
    {
        $field->fieldName = $request->post('fieldName');
        $field->faculty_id = $request->post('faculty');
        $field->okr_id = $request->post('okr');
        $field->user_id = Auth::user()->id;
        $field->save();
    }
}
