<?php

namespace App\Http\Controllers;

use App\Exports\manageOkrExport;
use App\Imports\manageOkrImport;
use App\okr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class OkrController extends Controller
{
    public function filterByOkr($id)
    {
        $okr['data'] = okr::find($id)->getFields();
        echo json_encode($okr);
        exit;
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'okrLevel' => 'required',
        ));

        $id = $request->post('okrName_id');

        if ($id) {
            $okr = okr::find($id);
            $this->setOkr($okr, $request);

            return redirect('/admin/manageOkr')->with('success', 'ОКР успiшно змiнено');
        }

        $okr = new okr();

        $this->setOkr($okr, $request);
        return redirect('/admin/manageOkr')->with('success', 'ОКР успiшно додано');
    }

    public function remove($id)
    {
        okr::find($id)->delete();

        return redirect()->back()->with('success', 'ОКР успiшно видалено');
    }

    public function import(Request $request)
    {
        $this->validate($request, array(
            'importFile' => 'required'
        ));

        Excel::import(new manageOkrImport(), $request->file('importFile'));

        return redirect()->back()->with('success', __('actoins.importSuccessMsg'));
    }

    public function export()
    {
        return Excel::download(new manageOkrExport(), 'manageOkr.xlsx');
    }

    private function setOkr($okr, $request)
    {
        $okr->okrLevel = $request->post('okrLevel');
        $okr->user_id = Auth::user()->id;
        $okr->save();
    }
}
