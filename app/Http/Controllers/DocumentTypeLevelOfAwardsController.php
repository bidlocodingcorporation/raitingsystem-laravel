<?php

namespace App\Http\Controllers;

use App\documentTypeLevelOfAwards;
use App\Exports\TypeLevelOfAwardsExport;
use App\Imports\TypeLevelOfAwardsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class DocumentTypeLevelOfAwardsController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, array(
            'documentTypeOfAward' => 'required|integer',
            'documentLevelOfAwards' => 'required|integer',
            'documentTypeLevelOfAwardsMark' => 'required|integer',
            'documentTypeLevelOfAwardsGroupPartisipationMark' => 'required|integer',
        ));

        $id = $request->post('documentTypeLevelOfAward_id');

        if ($id) {
            $documentTypeLevelOfAwards = documentTypeLevelOfAwards::find($id);
            $this->setDocumentTypeLevelOfAwards($documentTypeLevelOfAwards, $request);

            return redirect()->back()->with('success', 'Бали успiшно оновлено');
        }

        $documentTypeLevelOfAwards = new documentTypeLevelOfAwards();
        $this->setDocumentTypeLevelOfAwards($documentTypeLevelOfAwards, $request);

        return redirect('/admin/managePointsCalc')->with('success', 'Бали успiшно додано');
    }

    public function remove($id)
    {
        documentTypeLevelOfAwards::find($id)->delete();

        return redirect('/admin/managePointsCalc')->with('success', 'Пiдрахунок успiшно видалено');
    }

    public function import(Request $request)
    {
        $this->validate($request, array(
           'importFile' => 'required'
        ));

        Excel::import(new TypeLevelOfAwardsImport(), $request->file('importFile'));

        return redirect()->back()->with('success', __('actions.importSuccessMsg'));
    }

    public function export()
    {
        return Excel::download(new TypeLevelOfAwardsExport(), 'pointscalc.xlsx');
    }

    private function setDocumentTypeLevelOfAwards($documentTypeLevelOfAwards, $request)
    {
        $documentTypeLevelOfAwards->typeOfAwards_id = $request->post('documentTypeOfAward');
        $documentTypeLevelOfAwards->levelOfAwards_id = $request->post('documentLevelOfAwards');
        $documentTypeLevelOfAwards->documentMark = $request->post('documentTypeLevelOfAwardsMark');
        $documentTypeLevelOfAwards->groupPartisipationMark = $request->post('documentTypeLevelOfAwardsGroupPartisipationMark');
        $documentTypeLevelOfAwards->user_id = Auth::user()->id;
        $documentTypeLevelOfAwards->save();
    }
}
