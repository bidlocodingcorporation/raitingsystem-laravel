<?php

namespace App\Http\Controllers;

use App\documentLevelOfAwards;
use App\Exports\LevelOfAwardsExport;
use App\Imports\LevelOfAwardsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class DocumentLevelOfAwardsController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, array(
            'levelOfAwardName' => 'required',
        ));

        $id = $request->post('levelOfAward_id');
        if ($id) {
            $levelOfAward = documentLevelOfAwards::find($id);
            $this->setlevelOfAward($levelOfAward, $request);

            return redirect('/admin/manageDocumentLevelOfAwards')->with('success', __('Рiвень нагород успiшно змiнено'));
        }

        $levelOfAward = new documentLevelOfAwards();
        $this->setlevelOfAward($levelOfAward, $request);

        return redirect()->back()->with('success', __('Рiвень нагород успiшно додано'));
    }

    public function remove($id)
    {
        if ($id) {
            documentLevelOfAwards::find($id)->delete();

            return redirect()->back()->with('success', __('Рiвень нагород успiшно видалено'));
        }

        return redirect()->back()->with('error', __('Сталася помилка'));
    }

    public function import(Request $request)
    {
        $this->validate($request, array(
            'importFile' => 'required'
        ));

        Excel::import(new LevelOfAwardsImport(), $request->file('importFile'));

        return redirect()->back()->with('success', __('actions.importSuccessMsg'));
    }

    public function export()
    {
        return Excel::download(new LevelOfAwardsExport(), 'levelsofawards.xlsx');
    }

    private function setlevelOfAward($levelOfAward, $request)
    {
        $levelOfAward->documentLevelOfAward = $request->post('levelOfAwardName');
        $levelOfAward->user_id = Auth::user()->id;
        $levelOfAward->save();
    }
}
