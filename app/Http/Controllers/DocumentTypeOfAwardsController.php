<?php

namespace App\Http\Controllers;

use App\documentTypeOfAwards;
use App\Exports\TypeOfAwardsExport;
use App\Imports\TypeOfAwardsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class DocumentTypeOfAwardsController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, array(
            'typeOfAwardName' => ['required', 'string', 'max:255']
        ));

        if ($request->post('typeOfAward_id')) {
            $typeOfAward = documentTypeOfAwards::find($request->post('typeOfAward_id'));
            $this->_setTypeOfAward($typeOfAward, $request);

            return redirect()->back()->with('success', __('Тип нагород успiшно змінено'));
        }

        $typeOfAward = new documentTypeOfAwards();
        $this->_setTypeOfAward($typeOfAward, $request);

        return redirect()->back()->with('success', __('Тип нагород успішно зміненно'));
    }

    public function remove($id)
    {
        if ($id) {
            documentTypeOfAwards::find($id)->delete();

            return redirect()->back()->with('success', __('Тип нагород успiшно видалено'));
        }

        return redirect()->back()->with('error', __('Сталася помилка'));
    }

    public function import(Request $request)
    {
        $this->validate($request, array(
           'importFile' => 'required'
        ));

        Excel::import(new TypeOfAwardsImport(), $request->file('importFile'));

        return redirect()->back()->with('success', __('admin.importSuccessMsg'));
    }

    public function export()
    {
        return Excel::download(new TypeOfAwardsExport(), 'typeofawards.xlsx');
    }

    private function _setTypeOfAward($typeOfAward, $request)
    {
        $typeOfAward->documentAwardType = $request->post('typeOfAwardName');
        $typeOfAward->user_id = Auth::user()->id;
        $typeOfAward->save();
    }
}
