<?php

namespace App\Http\Controllers;

use App\document_image;
use App\Documents;
use App\documentTypeOfAwards;
use App\Events\NewDocumentCreatedEvent;
use App\Helpers\Helper;
use App\Mail\DocumentEmail;
use App\UserDocumentLogs;
use App\raitingConfig;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class DocumentsController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, array(
           'typeOfAward' => 'required',
           'levelOfAward' => 'required',
           'typeOfPartisipation' => 'required',
           'qty' => 'required|integer',
           'documentImages' => 'required'
        ));

        $document = new Documents();
        $document->qty = $request->input('qty');
        $document->documentLevelOfAwards_id = $request->input('levelOfAward');
        $document->documentTypeOfAwards_id = $request->input('typeOfAward');
        $document->documentTypeOfPartisipation_id = $request->input('typeOfPartisipation');
        $documentImages = $request->file('documentImages');

        if ($request->file('protocol')) {
            $document->protocol = $request->file('protocol')->store('uploads', 'public');
        }
        $document->user_id = Auth::user()->id;
        $document->save();

        foreach ($documentImages as $documentImage) {
            $image = new document_image();
            $image->document_id = $document->id;
            $image->documentImage = $documentImage->store('uploads', 'public');
            $image->save();
        }

        return redirect()->back()->with('success', __('text.documentAddedMessage'));
    }

    public function remove($id)
    {
        document_image::where('document_id', $id)->delete();
        Documents::find($id)->delete();

        return redirect()->back()->with('success', __('text.documentDeletedMessage'));
    }

    public function generatePdf(Request $request)
    {
        $kurs = $request->input('studyCource');
        $semestr = $request->input('studySemestr');
        $documents = Auth::user()->getDocuments();
        $helper = new Helper();
        $pdf = PDF::loadView('student.pdf.pdf', compact(array('documents', 'helper', 'kurs', 'semestr')));
        $images = array();
        foreach ($documents as $document) {
            foreach ($document->getImages() as $image) {
                if (!empty($image->documentImage)) {
                    $images[] = $image->documentImage;
                }
            }
        }

        event(new NewDocumentCreatedEvent(Auth::user(), $pdf, $images));
        //session()->flash('success', __('Документ також відправлено на електрону скриньку '. Auth::user()->email.' та на електрону скриньку вашого деканату'));

        //return $pdf->download('document.pdf');
        return redirect()->back()->with('success', __('Документ також відправлено на електрону скриньку '. Auth::user()->email.' та на електрону скриньку вашого деканату'));
    }

    public function getTypeOfAwardByLevelOfAward($id)
    {
        $document = new Documents();
        $result['data'] = $document->getTypeOfAwardByLevelOfAward($id);

        return response()->json($result);
    }
}
