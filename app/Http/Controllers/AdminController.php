<?php

namespace App\Http\Controllers;

use App\raitingConfig;
use Illuminate\Http\Request;
use App;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        $this->middleware('locale');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function manageDocuments()
    {
        return view('admin.managedocuments');
    }

    public function manageDocumentLevelOfAwards()
    {
        $levelOfAwards = App\documentLevelOfAwards::paginate(4);

        return view('admin.levelofaward.managedocumentlevelofawards', compact('levelOfAwards'));
    }

    public function editDocumentLevelOfAward($id)
    {
        $levelOfAward = App\documentLevelOfAwards::find($id);

        return view('admin.levelofaward.editdocumentlevelofaward', compact('levelOfAward'));
    }

    public function manageDocumentTypeOfAwards()
    {
        $typesOfAwards = App\documentTypeOfAwards::paginate(4);

        return view('admin.managedocumenttypeofawards', compact('typesOfAwards'));
    }

    public function manageDocumentTypeOfPartisipation()
    {
        $typesOfPartisipations = App\documentTypeOfPartisipation::paginate(6);

        return view('admin.managedocumenttypeofpartisipation', compact('typesOfPartisipations'));
    }

    public function managefields()
    {
        $okrs = App\okr::all();
        $faculties = App\faculty::all();
        $fields = App\field::paginate(4);

        return view('admin.fields.managefields', compact(array('fields', 'faculties', 'okrs')));
    }

    public function editfields($id)
    {
        $okrs = App\okr::all();
        $faculties = App\faculty::all();
        $field = App\field::find($id);

        return view('admin.fields.editfield', compact(array('field', 'faculties', 'okrs')));
    }

    public function managefactulties()
    {
        $factulties = App\faculty::paginate(5);

        return view('admin.faculty.managefaculties', compact('factulties'));
    }

    public function editfaculty($id)
    {
        $faculty = App\faculty::find($id);

        return view('admin.faculty.editfaculty', compact('faculty'));
    }

    public function manageusers()
    {
        $users = App\User::paginate(4);

        return view('admin.user.manageusers', compact('users'));
    }

    public function managepointscalc()
    {
        $documentTypeLevelOfAwards = App\documentTypeLevelOfAwards::paginate(4);
        $typesOfAwards = App\documentTypeOfAwards::all();
        $levelOfAwards = App\documentLevelOfAwards::all();

        return view('admin.pointscalc.managepointscalc', compact(array('documentTypeLevelOfAwards', 'typesOfAwards', 'levelOfAwards')));
    }

    public function addpointscalc()
    {
        $typesOfAwards = App\documentTypeOfAwards::all();
        $levelOfAwards = App\documentLevelOfAwards::all();

        return view('admin.pointscalc.addpointscalc', compact(array('typesOfAwards', 'levelOfAwards')));
    }

    public function manageokr()
    {
        $okrs = App\okr::all();

        return view('admin.okr.manageokr', compact('okrs'));
    }

    public function addokr()
    {
        return view('admin.okr.addokr');
    }

    public function editokr($id)
    {
        $okr = App\okr::find($id);

        return view('admin.okr.editokr', compact('okr'));
    }


    public function manageRaitingConfig()
    {
        $raitingConfig = raitingConfig::find(1);

        return view('admin.raitingconfig.manageraitingconfig', compact('raitingConfig'));
    }

    public function editRaitingConfig()
    {
        $raitingConfig = raitingConfig::find(1);

        return view('admin.raitingconfig.editraitingconfig', compact('raitingConfig'));
    }

    public function viewUserProfile($id)
    {
        $user = App\User::find($id);

        return view('admin.user.viewprofile', compact('user'));
    }

    public function lastActionsStatistic()
    {
        $userlogs = App\UserDocumentLogs::orderBy('id', 'desc')
            ->paginate(4);

        return view('admin.statistic.lastactionsstatistic', compact('userlogs'));
    }

    public function studentsUsingStatistic()
    {
        $faculties = App\faculty::paginate(4);
        return view('admin.statistic.studentsusing', compact('faculties'));
    }

    public function importUsers()
    {
        return view('admin.user.importusers');
    }
}
