<?php

namespace App\Http\Controllers;

use App\documentTypeOfPartisipation;
use App\Imports\TypeOfPartisipationImport;
use App\Exports\TypeOfPartisipationExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class DocumentTypeOfPartisipationController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, array(
            'typeOfPartisipationName' => 'required'
        ));

        if ($request->post('typeOfPartisipation_id')) {
            $id = $request->post('typeOfPartisipation_id');
            $typeOfPartisipation = documentTypeOfPartisipation::find($id);

            $this->setTypeOfPartisipation($typeOfPartisipation, $request);

            return redirect()->back()->with('success', __('Тип участi успiшно змiнено'));
        }

        if ($request->post('typeOfPartisipationName')) {
            $typeOfPartisipation = new documentTypeOfPartisipation();

            $this->setTypeOfPartisipation($typeOfPartisipation, $request);

            return redirect()->back()->with('success', __('Тип участi успiшно додано'));
        }

        return redirect()->back()->with('error', __('Сталася помилка'));
    }

    public function remove($id)
    {
        if ($id) {
            documentTypeOfPartisipation::find($id)->delete();

            return redirect()->back()->with('success', __('Тип участi успiшно видалено'));
        }

        return redirect()->back()->with('error', __('Сталася помилка'));
    }

    public function import(Request $request)
    {
        $this->validate($request, array(
           'importFile' => 'required'
        ));

        Excel::import(new TypeOfPartisipationImport(), $request->file('importFile'));

        return redirect()->back()->with('success', __('actions.importSuccessMsg'));
    }

    public function export()
    {
        return Excel::download(new TypeOfPartisipationExport(), 'typeofpartisipation.xlsx');
    }

    private function setTypeOfPartisipation($typeOfPartisipation, $request)
    {
        $typeOfPartisipation->documentTypeOfPartisipation = $request->post('typeOfPartisipationName');
        $typeOfPartisipation->user_id = Auth::user()->id;
        $typeOfPartisipation->save();
    }
}
