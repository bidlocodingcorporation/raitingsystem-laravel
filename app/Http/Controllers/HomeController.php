<?php

namespace App\Http\Controllers;

use App\documentLevelOfAwards;
use App\Documents;
use App\documentTypeOfAwards;
use App\documentTypeOfPartisipation;
use App\faculty;
use App\Helpers\Helper;
use App\okr;
use App\raitingConfig;
use App\UserDocumentLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('locale');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function createDocument()
    {
        $documents = Auth::user()->getDocuments();
        $documentTypesOfAwards = documentTypeOfAwards::all();
        $documentLevelOfAwards = documentLevelOfAwards::all();
        $documentTypesOfPartisipations = documentTypeOfPartisipation::all();
        $helper = new Helper();
        $config = raitingConfig::find(1);

        return view('student.createdocument', compact(array(
            'documents', 'documentLevelOfAwards','documentTypesOfAwards', 'documentTypesOfPartisipations', 'helper', 'config'
        )));
    }

    public function userProfile()
    {
        return view('student.profile');
    }

    public function editUserProfile()
    {
        $faculties = faculty::all();
        $okrs = okr::all();
        return view('student.editprofile', compact(array('faculties, okrs')));
    }
}
