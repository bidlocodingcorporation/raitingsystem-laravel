<?php

namespace App\Http\Controllers;

use App\Exports\manageRaitingConfigExport;
use App\Imports\manageRaitingConfigImport;
use App\raitingConfig;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class raitingConfigController extends Controller
{
    const RESPONSIBLE_PERSON_ID = 1;

    public function import(Request $request)
    {
        $this->validate($request, array(
            'importFile' => 'required'
        ));

        Excel::import(new manageRaitingConfigImport(), $request->file('importFile'));

        return redirect()->back()->with('success', __('actoins.importSuccessMsg'));
    }

    public function export()
    {
        return Excel::download(new manageRaitingConfigExport(), 'manageRaiting.xlsx');
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'responsiblePersonName' => 'required',
            'responsiblePersonEmail' => 'required',
            'documentsEmail' => 'required'
        ));

        $raitingConfig = raitingConfig::find(self::RESPONSIBLE_PERSON_ID);
        $raitingConfig->responsiblePersonName = $request->post('responsiblePersonName');
        $raitingConfig->responsiblePersonEmail = $request->post('responsiblePersonEmail');
        $raitingConfig->documentsEmail = $request->post('documentsEmail');
        $raitingConfig->save();

        return redirect('/admin/manageraitingconfig')->with('success', 'Конфiгурацiю успiшно змiнено');
    }
}
