<?php

namespace App\Http\Controllers;

use App\Exports\FacultyExport;
use App\faculty;
use App\Imports\FacultyImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class FacultyController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, array(
            'facultyName' => 'required'
        ));

        $id = $request->post('facultyId');

        if ($id) {
            $faculty = faculty::find($id);
            $this->setFaculty($faculty, $request);

            return redirect('/admin/manageFaculties')->with('success', __('Факультет успiшно додано'));
        }

        $faculty = new faculty();
        $this->setFaculty($faculty, $request);

        return redirect('/admin/manageFaculties')->with('success', __('Факультет успiшно додано'));
    }

    public function remove($id)
    {
        if ($id) {
            faculty::find($id)->delete();

            return redirect()->back()->with('success', __('Факультет успiшно видалено'));
        }

        return redirect()->back()->with('error', __('Сталася помилка'));
    }

    public function import(Request $request)
    {
        $this->validate($request, array(
           'importFile' => 'required'
        ));

        Excel::import(new FacultyImport(), $request->file('importFile'));

        return redirect()->back()->with('success', __('actions.importSuccessMsg'));
    }

    public function export()
    {
        return Excel::download(new FacultyExport(), 'faculties.xlsx');
    }

    private function setFaculty($faculty, $request)
    {
        $faculty->facultyName = $request->post('facultyName');
        $faculty->facultyEmail = $request->post('facultyEmail');
        $faculty->user_id = Auth::user()->id;
        $faculty->save();
    }
}
