<?php

namespace App\Http\Controllers;

use App\document_image;
use App\Documents;
use App\Imports\UsersImport;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function changeRole(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required',
            'userRole' => 'required'
        ]);

        if ($request->input('userId')) {
            $id = $request->input('userId');
            $user = User::find($id);
            $user->role_id = $request->input('userRole');
            $user->save();

            return redirect()->back()->with('success', __('Роль была успешно изменена'));
        }

        return redirect()->back()->with('error', __('Произошла ошибка'));
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, array(
           'userId' => 'required',
           'password' => 'required'
        ));

        if ($request->post('userId')) {
            $user = User::find($request->post('userId'));
            $user->password = Hash::make($request->input('password'));
            $user->save();
            return redirect()->back()->with('success', __('Пароль було успішно змінено'));
        }

        return redirect()->back()->with('error', __('Сталася помилка'));
    }

    public function remove($id)
    {
        Documents::where('user_id', $id)
            ->delete();
        User::find($id)
            ->delete();
        return redirect()->back()->with('success', __('Користувача успішно видалено'));
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'surname' => 'required',
            'secondName' => 'required',
            'faculty_id' => 'required',
            'field_id' => 'required',
            'okr_id' => 'required',
            'group' => 'required',
            'studentPhone' => 'required'
        ));

        $user = User::find(Auth::user()->id);
        $user->name = $request->post('name');
        $user->surname = $request->post('surname');
        $user->secondName = $request->post('secondName');
        $user->faculty_id = $request->post('faculty_id');
        $user->field_id = $request->post('field_id');
        $user->okr_id = $request->post('okr_id');
        $user->group = $request->post('group');
        $user->studentPhone = $request->post('studentPhone');
        $user->save();

        return redirect('/student/profile')->with('success', __('Дані профілю успішно змінено'));
    }

    public function import(Request $request)
    {
        $this->validate($request, array(
            'importFile' => 'required'
        ));

        Excel::import(new UsersImport(), $request->file('importFile'));

        return redirect('/admin/manageUsers')->with('success', __('admin.importSuccess'));
    }

    public function export()
    {
        return Excel::download(new UsersExport(), 'users.xlsx');
    }
}
