<?php

namespace App\Http\Controllers\Auth;

use App\faculty;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    const CREATE_DOCUMENT_URL = '/student/createDocument';

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = self::CREATE_DOCUMENT_URL;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'secondName' => ['required', 'string', 'max:255'],
            //'faculty' => ['required', 'integer', 'max:12'],
            'field' => ['required', 'integer'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'group' => ['required', 'string', 'max:15'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'studentPhone' => ['required', 'string', 'min:8']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'secondName' => $data['secondName'],
            'faculty_id' => $data['faculty'],
            'field_id' => $data['field'],
            'okr_id' => $data['okr'],
            'email' => $data['email'],
            'group' => $data['group'],
            'password' => Hash::make($data['password']),
            'studentPhone' => $data['studentPhone']
        ]);
    }
}
