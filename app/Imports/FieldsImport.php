<?php

namespace App\Imports;

use App\field;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class FieldsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return new field([
           'fieldName' => $row[1],
           'faculty_id' => $row[2],
           'okr_id' => $row[5]
        ]);
    }
}
