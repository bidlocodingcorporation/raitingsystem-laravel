<?php

namespace App\Imports;

use App\documentTypeOfAwards;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class TypeOfAwardsImport implements ToModel
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        return new documentTypeOfAwards([
            'documentAwardType' => $row[1]
        ]);
    }
}
