<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\User;

class UsersImport implements ToModel
{
    /**
    * @param Array $row
    */
    public function model(array $row)
    {
        return new User([
            'name' => $row[0],
            'surname' => $row[1],
            'secondName' => $row[2],
            'faculty_id' => $row[3],
            'field_id' => $row[4],
            'okr_id' => $row[5],
            'group' => $row[6],
            'email' => $row[7],
            'image' => $row[8],
            'password' => md5($row[9]),
            'studentPhone' => $row[10],
            'role_id' => $row[11]
        ]);
    }
}
