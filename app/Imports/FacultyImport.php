<?php

namespace App\Imports;

use App\faculty;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class FacultyImport implements ToModel
{

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return new faculty([
           'facultyName' => $row[1],
           'facultyEmail' => $row[4]
        ]);
    }
}
