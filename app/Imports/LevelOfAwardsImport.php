<?php

namespace App\Imports;

use App\documentLevelOfAwards;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class LevelOfAwardsImport implements ToModel
{
    /**
    * @param Collection $collection
    */

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return new documentLevelOfAwards([
           'documentLevelOfAward' => $row[1]
        ]);
    }
}
