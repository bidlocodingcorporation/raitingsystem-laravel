<?php

namespace App\Imports;

use App\raitingConfig;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class manageRaitingConfigImport implements ToModel
{

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return new raitingConfig([
            'greetingMessage' => $row[1],
            'responsiblePersonName' => $row[2],
            'responsiblePersonEmail' => $row[3],
            'documentsEmail' => $row[4]
        ]);
    }
}
