<?php

namespace App\Imports;

use App\okr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class manageOkrImport implements ToModel
{

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return new okr([
            'okrLevel' => $row[1]
        ]);
    }
}
