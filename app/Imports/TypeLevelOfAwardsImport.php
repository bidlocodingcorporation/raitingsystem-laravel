<?php

namespace App\Imports;

use App\documentTypeLevelOfAwards;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class TypeLevelOfAwardsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return new documentTypeLevelOfAwards([
           'typeOfAwards_id' => $row[1],
           'levelOfAwards_id' => $row[2],
           'documentMark' => $row[3],
           'groupPartisipationMark' => $row[4]
        ]);
    }
}
