<?php

namespace App\Imports;

use App\documentTypeOfPartisipation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class TypeOfPartisipationImport implements ToModel
{

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return new documentTypeOfPartisipation([
           'documentTypeOfPartisipation' => $row[1]
        ]);
    }
}
