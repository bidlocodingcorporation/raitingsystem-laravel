<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class documentTypeOfAwards extends Model
{
    protected $hidden = ['user_id'];
    protected $fillable = ['documentAwardType', 'created_at', 'updated_at'];
}
