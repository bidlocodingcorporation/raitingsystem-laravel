<?php

namespace App;

use App\Helpers\Helper;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{

    private const CONGREGATE_DOCUMENT_TYPE_OF_PARTISIPATION_ID = 2;

    public function documentLevelOfAwards()
    {
        return $this->hasOne('App\documentLevelOfAwards','id', 'documentLevelOfAwards_id');
    }

    public function documentTypeOfAwards()
    {
        return $this->hasOne('App\documentTypeOfAwards', 'id' , 'documentTypeOfAwards_id');
    }

    public function documentTypeOfPartisipation()
    {
        return $this->hasOne('App\documentTypeOfPartisipation' ,'id', 'documentTypeOfPartisipation_id');
    }

    public function documentImage()
    {
        return $this->hasMany('App\document_image','document_id');
    }

    public function getImages()
    {
        if ($this->documentImage) {
            return $this->documentImage;
        }

        return 0;
    }

    public function getDocumentLevelOfAwards()
    {
        return $this->documentLevelOfAwards->documentLevelOfAward;
    }

    public function getDocumentTypeOfAwards()
    {
        return $this->documentTypeOfAwards->documentAwardType;
    }

    public function getTypeOfPartisipation()
    {
        return $this->documentTypeOfPartisipation->documentTypeOfPartisipation;
    }

    public function getDocumentMark()
    {
        $mark = documentTypeLevelOfAwards::where('typeOfAwards_id', $this->documentTypeOfAwards_id)
            ->where('levelOfAwards_id', $this->documentLevelOfAwards_id)
            ->get()
            ->first();

        if (empty($mark)) {
            return 0;
        }

        if ($this->documentTypeOfPartisipation->id == self::CONGREGATE_DOCUMENT_TYPE_OF_PARTISIPATION_ID) {
            return $mark->groupPartisipationMark*$this->qty;
        }

        return $mark->documentMark*$this->qty;
    }

    public function getTypeOfAwardByLevelOfAward($levelOfAwardId)
    {
        $documentTypeLevelOfAwards = documentTypeLevelOfAwards::where('levelOfAwards_id', $levelOfAwardId)
            ->get()
            ->pluck('typeOfAwards_id');

        return documentLevelOfAwards::find($documentTypeLevelOfAwards);
    }

    public function getImageSizeLimit()
    {
        return ini_get('upload_max_filesize');
    }
}
