<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'secondName', 'faculty_id', 'field_id', 'email', 'group', 'password', 'studentPhone', 'okr_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'faculty_id' => 'integer',
        'field_id' => 'integer'
    ];

    private const ADMIN_USER_GROUP = 1;
    private const STUDENT_USER_GROUP = 2;

    public function documents()
    {
        return $this->hasMany('App\Documents', 'user_id', 'id');
    }

    public function userDocumentLogs()
    {
        return $this->hasMany('App\UserDocumentLogs', 'user_id', 'id');
    }

    public function faculty()
    {
        return $this->hasOne('App\faculty', 'id', 'faculty_id');
    }

    public function field()
    {
        return $this->hasOne('App\field', 'id', 'field_id');
    }

    public function role()
    {
        return $this->hasOne('App\role', 'id', 'role_id');
    }

    public function logs()
    {
        return $this->hasMany('App\UserDocumentLogs', 'user_id');
    }

    public function okr()
    {
        return $this->hasOne('App\okr', 'id', 'okr_id');
    }

    public function getOkr()
    {
        return $this->okr->okrLevel;
    }

    public function getLoggedDocuments()
    {
        return $this->logs;
    }

    public function getDocuments()
    {
        return $this->documents;
    }

    public function getDocumentLogs()
    {
        return $this->userDocumentLogs;
    }

    public function getUsersFaculty()
    {
        return $this->faculty->facultyName;
    }

    public function getUsersFacultyEmail()
    {
        return $this->faculty->facultyEmail;
    }

    public function getUsersField()
    {
        return $this->field->fieldName;
    }

    public function getRole()
    {
        return $this->role->roleName;
    }

    public function ifAdmin()
    {
        if ($this->role_id === self::ADMIN_USER_GROUP) {
            return true;
        }

        return false;
    }

    public function getUserRoles()
    {
        return role::all();
    }

    public function isMadeDocument()
    {
        if ($this->userDocumentLogs) {
            return true;
        }

        return false;
    }
}
