<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class faculty extends Model
{
    protected $fillable = ['facultyName', 'facultyEmail'];

    public function fields()
    {
        return $this->hasMany('App\field', 'faculty_id', 'id');
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function students()
    {
        return $this->hasMany('App\User', 'faculty_id', 'id');
    }

    public function getSystemUsersValue()
    {
        $systemUsers = 0;

        foreach ($this->students as $student) {
            if ($student->userDocumentLogs) {
                $systemUsers++;
            }
        }

        return $systemUsers;
    }

    public function getSystemUsersSurnames()
    {
        $surnames = array();

        foreach ($this->students as $student) {
            if ($student->userDocumentLogs) {
                $surnames[] = $student->surname;
            }
        }

        return $surnames;
    }
}
