<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class okr extends Model
{
    protected $fillable = ['okrLevel'];

    public function fields()
    {
        return $this->hasMany('App\fields', 'okr_id', 'id');
    }

    public function getFields()
    {
        return $this->fields;
    }
}
