<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class field extends Model
{
    protected $fillable = ['fieldName', 'faculty_id', 'okr_id'];

    public function faculty()
    {
        return $this->hasOne('App\faculty', 'id', 'faculty_id');
    }

    public function okr()
    {
        return $this->hasOne('App\okr', 'id', 'okr_id');
    }

    public function getFaculty()
    {
        if ($this->faculty_id == NULL) {
            return NULL;
        }

        return $this->faculty->facultyName;
    }

    public function getOkr()
    {
        if ($this->okr_id == NULL) {
            return NULL;
        }

        return $this->okr->okrLevel;
    }
}
