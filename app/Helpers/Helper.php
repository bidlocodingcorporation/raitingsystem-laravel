<?php

namespace App\Helpers;

use App\Documents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Helper extends Model
{
    public const POINTS_LIMIT = 100;

    public function getTotalMarks()
    {
        $documents = Auth::user()->getDocuments();
        $sum = 0;

        foreach ($documents as $document) {
            $sum += $document->getDocumentMark();
            if ($sum > self::POINTS_LIMIT) {
                return self::POINTS_LIMIT;
            }
        }

        return $sum;
    }

    public function getPastYear()
    {
        return date('Y')-1;
    }
}
