<?php

namespace App\Exports;

use App\okr;
use Maatwebsite\Excel\Concerns\FromCollection;

class manageOkrExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return okr::all();
    }
}
