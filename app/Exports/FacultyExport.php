<?php

namespace App\Exports;

use App\faculty;
use Maatwebsite\Excel\Concerns\FromCollection;

class FacultyExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return faculty::all();
    }
}
