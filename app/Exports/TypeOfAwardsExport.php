<?php

namespace App\Exports;

use App\documentTypeOfAwards;
use Maatwebsite\Excel\Concerns\FromCollection;

class TypeOfAwardsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return documentTypeOfAwards::all();
    }
}
