<?php

namespace App\Exports;

use App\raitingConfig;
use Maatwebsite\Excel\Concerns\FromCollection;

class manageRaitingConfigExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return raitingConfig::all();
    }
}
