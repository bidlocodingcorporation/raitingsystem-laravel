<?php

namespace App\Exports;

use App\field;
use Maatwebsite\Excel\Concerns\FromCollection;

class FieldsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return field::all();
    }
}
