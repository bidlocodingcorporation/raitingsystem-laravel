<?php

namespace App\Exports;

use App\documentTypeOfPartisipation;
use Maatwebsite\Excel\Concerns\FromCollection;

class TypeOfPartisipationExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return documentTypeOfPartisipation::all();
    }
}
