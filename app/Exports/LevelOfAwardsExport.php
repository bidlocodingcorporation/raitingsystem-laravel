<?php

namespace App\Exports;

use App\documentLevelOfAwards;
use Maatwebsite\Excel\Concerns\FromCollection;

class LevelOfAwardsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return documentLevelOfAwards::all();
    }
}
