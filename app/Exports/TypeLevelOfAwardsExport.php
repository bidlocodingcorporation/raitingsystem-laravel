<?php

namespace App\Exports;

use App\documentTypeLevelOfAwards;
use Maatwebsite\Excel\Concerns\FromCollection;

class TypeLevelOfAwardsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return documentTypeLevelOfAwards::all();
    }
}
