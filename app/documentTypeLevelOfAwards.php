<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class documentTypeLevelOfAwards extends Model
{
    protected $fillable = ['typeOfAwards_id', 'levelOfAwards_id', 'documentMark', 'groupPartisipationMark'];

    public function levelofaward()
    {
        return $this->hasOne('App\documentLevelOfAwards', 'id', 'levelOfAwards_id');
    }

    public function typeofaward()
    {
        return $this->hasOne('App\documentTypeOfAwards', 'id', 'typeOfAwards_id');
    }

    public function getLevelOfAwards()
    {
        return $this->levelofaward->documentLevelOfAward;
    }

    public function getTypeOfAward()
    {
        return $this->typeofaward->documentAwardType;
    }
}
