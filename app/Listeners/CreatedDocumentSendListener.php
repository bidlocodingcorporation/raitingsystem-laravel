<?php

namespace App\Listeners;

use App\Mail\DocumentEmail;
use App\raitingConfig;
use App\UserDocumentLogs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class CreatedDocumentSendListener
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $message = new DocumentEmail();
        $message->attachData($event->pdfDocument->output(), 'document.pdf');

        foreach ($event->images as $image) {
            $message->attach(public_path().'/storage/'.$image);
        }

        $this->sendMail($event->user->email, $message);

        if ($event->user->getUsersFacultyEmail()) {
            $this->sendMail($event->user->getUsersFacultyEmail(), $message);
        }

        $educationalDepartmentEmail = raitingConfig::find(1)
            ->documentsEmail;

        if ($educationalDepartmentEmail) {
            $this->sendMail($educationalDepartmentEmail, $message);
        }

        UserDocumentLogs::create([
            'status' => UserDocumentLogs::SENDED_STATUS,
            'user_id' => $event->user->id
        ]);
    }

    private function sendMail($userEmail, $message)
    {
        Mail::to($userEmail)->send($message);
    }
}
