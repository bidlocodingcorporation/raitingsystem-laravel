#!/usr/bin/env bash
composer install --ignore-platform-reqs
composer dump_autoload
npm install
npm run dev
php artisan migrate
php artisan db:seed
