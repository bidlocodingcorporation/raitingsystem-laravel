<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;

Route::get('/', function () {
    $raitingConfig = \App\raitingConfig::find(1);
    return view('welcome', compact('raitingConfig'));
})->middleware('locale');

Route::get('/filterByFaculty/{id}', 'FieldController@getFieldsByFaculty');

Route::get('/admin', 'AdminController@index');
Route::get('/admin/manageFields', 'AdminController@managefields');
Route::get('/admin/manageFaculties', 'AdminController@managefactulties');
Route::get('/admin/manageFaculties/edit/{id}', 'AdminController@editfaculty');
Route::get('/admin/manageDocuments', 'AdminController@manageDocuments');
Route::get('/admin/manageUsers', 'AdminController@manageusers');
Route::get('/admin/managePointsCalc', 'AdminController@managepointscalc');
Route::get('/admin/managePointsCalc/add', 'AdminController@addpointscalc');
Route::get('/admin/managePointsCalc/remove/{id}', 'DocumentTypeLevelOfAwardsController@remove');
Route::post('/admin/managePointsCalc/store', 'DocumentTypeLevelOfAwardsController@store');
Route::post('/admin/managePointsCalc/import', 'DocumentTypeLevelOfAwardsController@import');
Route::get('/admin/managePointsCalc/export', 'DocumentTypeLevelOfAwardsController@export');

Route::post('/admin/manageUsers/changeRole', 'UserController@changeRole');
Route::post('/admin/manageUsers/changePassword', 'UserController@changePassword');
Route::get('/admin/manageUsers/remove/{id}', 'UserController@remove');
Route::get('/admin/manageUsers/viewprofile/{id}', 'AdminController@viewUserProfile');
Route::get('/admin/manageUsers/importUsers', 'AdminController@importUsers');
Route::get('/admin/manageUsers/exportUsers', 'UserController@export');
Route::post('/admin/manageUsers/importUsers/import', 'UserController@import');

Route::get('/admin/manageDocumentLevelOfAwards', 'AdminController@manageDocumentLevelOfAwards');
Route::get('/admin/manageDocumentLevelOfAwards/edit/{id}', 'AdminController@editDocumentLevelOfAward');
Route::get('/admin/manageDocumentLevelOfAwards/remove/{documentLevelAward}', 'DocumentLevelOfAwardsController@remove');
Route::post('/admin/manageDocumentLevelOfAwards/store', 'DocumentLevelOfAwardsController@store');
Route::post('/admin/manageDocumentLevelOfAwards/import', 'DocumentLevelOfAwardsController@import');
Route::get('/admin/manageDocumentLevelOfAwards/export', 'DocumentLevelOfAwardsController@export');

Route::get('/admin/manageDocumentTypeOfAwards', 'AdminController@manageDocumentTypeOfAwards');
Route::get('/admin/manageDocumentTypeOfAwards/remove/{documentTypeOfAward}', 'DocumentTypeOfAwardsController@remove');
Route::post('/admin/manageDocumentTypeOfAwards/store', 'DocumentTypeOfAwardsController@store');
Route::post('/admin/manageDocumentTypeOfAwards/importTypeOfAwards', 'DocumentTypeOfAwardsController@import');
Route::get('/admin/manageDocumentTypeOfAwards/exportTypeOfAwards', 'DocumentTypeOfAwardsController@export');

Route::get('/admin/manageDocumentTypeOfPartisipation', 'AdminController@manageDocumentTypeOfPartisipation');
Route::get('/admin/manageDocumentTypeOfPartisipation/remove/{documentTypeOfPartisipation}', 'DocumentTypeOfPartisipationController@remove');
Route::post('/admin/manageDocumentTypeOfPartisipation/store', 'DocumentTypeOfPartisipationController@store');
Route::post('/admin/manageDocumentTypeOfPartisipation/import', 'DocumentTypeOfPartisipationController@import');
Route::get('/admin/manageDocumentTypeOfPartisipation/export', 'DocumentTypeOfPartisipationController@export');

Route::get('/admin/manageDocumentTypeOfPartisipation', 'AdminController@manageDocumentTypeOfPartisipation');

Route::get('/admin/manageFaculties/remove/{faculty}', 'FacultyController@remove');
Route::post('/admin/manageFaculties/save', 'FacultyController@store');
Route::post('/admin/manageFaculties/import', 'FacultyController@import');
Route::get('/admin/manageFaculties/export', 'FacultyController@export');

Route::get('/admin/manageFields/remove/{field}', 'FieldController@remove');
Route::get('/admin/manageFields/edit/{field}', 'AdminController@editfields');
Route::post('/admin/manageFields/save', 'FieldController@store');
Route::post('/admin/manageFields/import', 'FieldController@import');
Route::get('/admin/manageFields/export', 'FieldController@export');

Route::get('/admin/manageOkr', 'AdminController@manageokr');
Route::get('/admin/manageOkr/add', 'AdminController@addokr');
Route::get('/admin/manageOkr/edit/{id}', 'AdminController@editokr');
Route::get('/admin/manageOkr/remove/{id}', 'OkrController@remove');
Route::post('/admin/manageOkr/store', 'OkrController@store');
Route::post('/admin/manageOkr/import', 'OkrController@import');
Route::get('/admin/manageOkr/export', 'OkrController@export');

Route::get('/admin/manageraitingconfig', 'AdminController@manageRaitingConfig');
Route::get('/admin/manageraitingconfig/edit', 'AdminController@editRaitingConfig');
Route::post('/admin/manageraitingconfig/store', 'raitingConfigController@store');
Route::post('/admin/manageraitingconfig/import', 'raitingConfigController@import');
Route::get('/admin/manageraitingconfig/export', 'raitingConfigController@export');

Route::get('/student/createDocument', 'HomeController@createDocument');
Route::get('/student/profile', 'HomeController@userProfile');
Route::get('/student/profile/updateProfile', 'HomeController@editUserProfile');
Route::post('/student/profile/updateProfile/store', 'UserController@updateProfile');

Route::get('/getTypeOfAwardsByLevel/{id}', 'DocumentsController@getTypeOfAwardByLevelOfAward');

Route::get('/student/createDocument/remove/{document}', 'DocumentsController@remove');
Route::get('/student/createDocument/generatePDF', 'DocumentsController@generatePdf');
Route::post('/student/createDocument/store', 'DocumentsController@store');

Route::get('/admin/lastActionsStatistic', 'AdminController@lastActionsStatistic');
Route::get('/admin/studentsUsingStatistic', 'AdminController@studentsUsingStatistic');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/setLocale/{lang}', function ($lang) {
    session()->put('locale', $lang);
    return redirect()->back();
})->middleware('admin');
