@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                @include('layouts.messages')
                <form method="POST" action="{{URL::to('/admin/manageDocumentLevelOfAwards/store')}}">
                    <div class="padding-form">
                        <input type="hidden" value="{{ $levelOfAward->id }}" name="levelOfAward_id">
                        <label for="levelOfAwardName">{{__('admin.nameOfTheLevelOfAwards')}}</label>
                        <input type="text" class="form-control" name="levelOfAwardName"
                               value="{{ $levelOfAward->documentLevelOfAward }}" required>
                        <button type="submit" class="btn btn-primary btn-margin">{{__('actions.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
