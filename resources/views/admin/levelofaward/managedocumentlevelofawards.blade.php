@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.managementOfAwardLevels')}}</h3></div>
                @include('layouts.messages')
                <div class="control-panel">
                    <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#addLevelOfAwardModal"
                            title="{{__('admin.addANewLevelOfRewards')}}">{{__('actions.add')}}+
                    </button>
                    <button type="button" class="btn btn-danger">{{__('actions.delete')}}</button>
                    <div class="modal fade" id="addLevelOfAwardModal" tabindex="-1" role="dialog"
                         aria-labelledby="addLevelOfAwardModalLevel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="addFacultyModalLabel">{{__('admin.addLevelOfRewards')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageDocumentLevelOfAwards/store')}}">
                                    <div class="modal-body">
                                        <label for="facultyName">{{__('admin.nameOfTheLevelOfAwards')}}</label>
                                        <input type="text" class="form-control" name="levelOfAwardName" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="#" title="{{ __('admin.importLevelOfAwards') }}" class="btn btn-info" data-toggle="modal" data-target="#importLevelOfAwardModal">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </a>
                    <div class="modal fade" id="importLevelOfAwardModal" tabindex="-1" role="dialog"
                         aria-labelledby="importLevelOfAwardModalLevel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="addFacultyModalLabel">{{__('admin.importLevelOfAwards')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageDocumentLevelOfAwards/import')}}" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <label for="importFile">{{ __('admin.uploadFile') }}</label>
                                        <input type="file" class="form-control" name="importFile">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.import')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="{{ URL::to('/admin/manageDocumentLevelOfAwards/export') }}" title="{{ __('admin.exportLevelOfAwards') }}" class="btn btn-info">
                        <i class="fas fa-cloud-download-alt"></i>
                    </a>
                </div>
                <table cellpadding="10" class="table table-levels-awards table-bordered table-font table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <tr>
                        <th class="t-fields">{{__('actions.highlight')}}</th>
                        <th class="t-fields">{{__('admin.nameOfTheLevelOfAwards')}}</th>
                        <th class="t-fields">{{__('actions.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (isset($levelOfAwards))
                        @foreach ($levelOfAwards as $levelOfAward)
                            <tr>
                                <td class="t-fields-awards"><input type="checkbox"></td>
                                <td class="t-fields-awards">{{$levelOfAward->documentLevelOfAward}}</td>
                                <td class="t-fields-awards">
                                    <div class="h-action action">
                                        <a href="{{URL::to('/admin/manageDocumentLevelOfAwards/remove/'.$levelOfAward->id)}}"
                                           class="btn btn-danger btn-new"><i class="fas fa-trash-alt"></i></a>
                                        <a href="{{URL::to('/admin/manageDocumentLevelOfAwards/edit/'.$levelOfAward->id)}}"
                                           class="btn btn-info btn-new text-white bg-primary"><i
                                                class="fas fa-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <div class="navigate">
                        {{ $levelOfAwards->links() }}
                    </div>
                </table>
            </div>
        </div>
    </div>
@endsection
