@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.managementOfTypesOfAwards')}}</h3></div>
                @include('layouts.messages')
                <div class="control-panel">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addTypeOfAwardModal"
                            title="{{__('admin.addTypeOfAward')}}">{{__('actions.add')}}+
                    </button>
                    <button type="button" class="btn btn-danger">{{__('actions.delete')}}</button>
                    <div class="modal fade" id="addTypeOfAwardModal" tabindex="-1" role="dialog"
                         aria-labelledby="addTypeOfAwardModalLevel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="addTypeModalLabel">{{__('admin.addTypeOfAwards')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageDocumentTypeOfAwards/store')}}">
                                    <div class="modal-body">
                                        <label for="facultyName">{{__('admin.nameOfTheTypeOfAwards')}}</label>
                                        <input type="text" class="form-control" name="typeOfAwardName" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="btn btn-info" data-toggle="modal" data-target="#importTypeOfAwardsModal">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </a>
                    <div class="modal fade" id="importTypeOfAwardsModal" tabindex="-1" role="dialog"
                         aria-labelledby="importTypeOfAwardsModalLevel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="importTypeOfAwardsModalLabel">{{__('admin.importTypeOfAward')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageDocumentTypeOfAwards/importTypeOfAwards')}}" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <label for="facultyName">{{__('admin.uploadFile')}}</label>
                                        <input type="file" class="form-control" name="importFile" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.import')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="{{ URL::to('/admin/manageDocumentTypeOfAwards/exportTypeOfAwards') }}" class="btn btn-info">
                        <i class="fas fa-cloud-download-alt"></i>
                    </a>
                </div>
                <table cellpadding="10" class="table table-levels-awards table-bordered table-font table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <tr>
                        <th class="t-fields">{{__('actions.highlight')}}</th>
                        <th class="t-fields">{{__('admin.nameOfTheTypeOfAwards')}}</th>
                        <th class="t-fields">{{__('actions.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (isset($typesOfAwards))
                        @foreach ($typesOfAwards as $typeOfAward)
                            <tr>
                                <td class="t-fields-awards"><input type="checkbox"></td>
                                <td class="t-fields-awards">{{$typeOfAward->documentAwardType}}</td>
                                <td class="t-fields-awards">
                                    <div class="h-action action">
                                        <a href="{{URL::to('/admin/manageDocumentTypeOfAwards/remove/'.$typeOfAward->id)}}"
                                           class="btn btn-danger btn-new"><i class="fas fa-trash-alt"></i></a>
                                        <a href="#" data-toggle="modal" data-target="#editTypeOfAwardsModal"
                                           class="btn btn-info btn-new text-white bg-primary"><i
                                                class="fas fa-edit"></i></a>
                                        <div class="modal fade" id="editTypeOfAwardsModal" tabindex="-1"
                                             role="dialog" aria-labelledby="editTypeOfAwardsModalLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="editTypeOfPartisipationLabel">{{__('admin.editTheTypeOfAward')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form method="POST"
                                                          action="{{URL::to('/admin/manageDocumentTypeOfAwards/store')}}">
                                                        <div class="modal-body">
                                                            <input type="hidden" value="{{ $typeOfAward->id }}"
                                                                   name="typeOfAward_id" class="form-control">
                                                            <label for="facultyName">{{__('admin.nameOfTheTypeOfParticipation')}}</label>
                                                            <input type="text" class="form-control"
                                                                   name="typeOfAwardName"
                                                                   value="{{ $typeOfAward->documentAwardType }}"
                                                                   required>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">{{__('actions.close')}}</button>
                                                            <button type="submit"
                                                                    class="btn btn-primary">{{__('actions.save')}}</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <div class="navigate">
                        {{ $typesOfAwards->links() }}
                    </div>
                </table>
            </div>
        </div>
    </div>
@endsection
