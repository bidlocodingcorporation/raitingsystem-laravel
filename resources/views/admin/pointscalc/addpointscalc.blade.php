@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                @include('layouts.messages')
                <form method="post" action="{{ URL::to('/admin/managePointsCalc/store') }}">
                    <div class="padding-form">
                        <label for="documentTypeOfAward">{{ __('admin.nameOfTheTypeOfAwards') }}</label>
                        <select class="form-control" name="documentTypeOfAward">
                            @foreach($typesOfAwards as $typeOfAward)
                                <option value="{{ $typeOfAward->id }}">{{ $typeOfAward->documentAwardType }}</option>
                            @endforeach
                        </select>
                        <label for="documentLevelOfAwards">{{__('admin.nameOfTheLevelOfAwards')}}</label>
                        <select class="form-control" name="documentLevelOfAwards">
                            @foreach($levelOfAwards as $levelOfAward)
                                <option
                                    value="{{ $levelOfAward->id }}">{{ $levelOfAward->documentLevelOfAward }}</option>
                            @endforeach
                        </select>
                        <label for="documentTypeLevelOfAwardsMark">{{__('admin.pointsForIndividualParticipation')}}</label>
                        <input type="text" class="form-control" name="documentTypeLevelOfAwardsMark">
                        <label
                            for="documentTypeLevelOfAwardsGroupPartisipationMark">{{__('admin.pointsForCollectiveParticipation')}}</label>
                        <input type="text" class="form-control" name="documentTypeLevelOfAwardsGroupPartisipationMark">
                        <button type="submit" class="btn btn-primary btn-margin">{{__('actions.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
