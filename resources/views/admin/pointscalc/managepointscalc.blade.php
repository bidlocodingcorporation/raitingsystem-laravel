@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.scoring')}}</h3></div>
                @include('layouts.messages')
                <table cellpadding="10" class="table table-bordered table-font table-points table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <tr>
                        <th class="t-fields">{{__('admin.nameOfTheTypeOfAwards')}}</th>
                        <th class="t-fields">{{__('admin.nameOfTheLevelOfAwards') }}</th>
                        <th class="t-fields">{{__('admin.pointsForIndividualParticipation')}}</th>
                        <th class="t-fields">{{__('admin.pointsForCollectiveParticipation')}}</th>
                        <th class="t-fields">{{__('actions.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <div class="control-panel top-panel">
                        <a href="{{ URL::to('/admin/managePointsCalc/add') }}" class="btn btn-success" title="{{ __('actions.add') }}">{{ __('actions.add') }}</a>
                        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#importPointsCalc">
                            <i class="fas fa-cloud-upload-alt"></i>
                        </a>
                        <div class="modal fade" id="importPointsCalc" tabindex="-1" role="dialog"
                             aria-labelledby="importPointsCalcLevel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="importPointsCalcLabel">{{__('admin.importPointsCalc')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="POST" action="{{URL::to('/admin/managePointsCalc/import')}}" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <label for="typeOfPartisipation">{{__('admin.uploadFile')}}</label>
                                            <input type="file" class="form-control" name="importFile" required>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">{{__('actions.close')}}</button>
                                            <button type="submit" class="btn btn-primary">{{__('actions.import')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a href="{{ URL::to('/admin/managePointsCalc/export') }}" class="btn btn-info">
                            <i class="fas fa-cloud-download-alt"></i>
                        </a>
                    </div>
                    @foreach($documentTypeLevelOfAwards as $documentTypeLevelOfAward)
                        <tr>
                            <td class="t-fields-awards">{{ $documentTypeLevelOfAward->getTypeOfAward() }}</td>
                            <td class="t-fields-awards">{{ $documentTypeLevelOfAward->getLevelOfAwards() }}</td>
                            <td class="t-fields-awards">{{ $documentTypeLevelOfAward->documentMark }}</td>
                            <td class="t-fields-awards">{{ $documentTypeLevelOfAward->groupPartisipationMark }}</td>
                            <td class="t-fields-awards">
                                <div class="h-action action">
                                    <a href="#" data-toggle="modal"
                                       data-target="#editPointsCalcModal{{ $documentTypeLevelOfAward->id }}"
                                       class="btn btn-info btn-new text-white bg-primary"><i
                                            class="fas fa-edit"></i></a>
                                    <a href="{{ URL::to('/admin/managePointsCalc/remove/'.$documentTypeLevelOfAward->id) }}"
                                       class="btn btn-danger btn-new"><i class="fas fa-trash-alt"></i></a>
                                </div>
                                <div class="modal fade" id="editPointsCalcModal{{ $documentTypeLevelOfAward->id }}"
                                     tabindex="-1" role="dialog"
                                     aria-labelledby="editPointsCalcModalLabel{{ $documentTypeLevelOfAward->id }}"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">{{__('actions.edit')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="{{URL::to('/admin/managePointsCalc/store')}}">
                                                <div class="modal-body">
                                                    <input type="hidden" value="{{ $documentTypeLevelOfAward->id }}"
                                                           name="documentTypeLevelOfAward_id">
                                                    <label for="documentTypeOfAward">{{ __('admin.nameOfTheTypeOfAwards') }}</label>
                                                    <select class="form-control" name="documentTypeOfAward"
                                                            onchange="getLevelOfAward(this.value)">
                                                        @foreach($typesOfAwards as $typeOfAward)
                                                            <option
                                                                value="{{ $typeOfAward->id }}">{{ $typeOfAward->documentAwardType }}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="documentLevelOfAwards">{{__('admin.nameOfTheLevelOfAwards')}}</label>
                                                    <select class="form-control" name="documentLevelOfAwards">
                                                        @foreach($levelOfAwards as $levelOfAward)
                                                            <option
                                                                value="{{ $levelOfAward->id }}">{{ $levelOfAward->documentLevelOfAward }}</option>
                                                        @endforeach
                                                    </select>
                                                    <label
                                                        for="documentTypeLevelOfAwardsMark">{{__('admin.pointsForIndividualParticipation')}}</label>
                                                    <input type="text" class="form-control"
                                                           value="{{ $documentTypeLevelOfAward->documentMark }}"
                                                           name="documentTypeLevelOfAwardsMark">
                                                    <label
                                                        for="documentTypeLevelOfAwardsGroupPartisipationMark">{{__('admin.pointsForCollectiveParticipation')}}</label>
                                                    <input type="text" class="form-control"
                                                           value="{{ $documentTypeLevelOfAward->groupPartisipationMark }}"
                                                           name="documentTypeLevelOfAwardsGroupPartisipationMark">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">{{__('actions.close')}}</button>
                                                    <button type="submit"
                                                            class="btn btn-primary">{{__('actions.save')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="navigate">
                    {{ $documentTypeLevelOfAwards->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
