@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.userManagement')}}</h3></div>
                @include('layouts.messages')
                <div class="control-panel">
                    <a href="{{ URL::to('/admin/manageUsers/importUsers') }}" class="btn btn-info btn-new-manage" title="{{ __('admin.importUsers') }}">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </a>
                    <a href="{{ URL::to('/admin/manageUsers/exportUsers') }}" class="btn btn-info btn-new-manage" title="{{ __('admin.exportUsers') }}">
                        <i class="fas fa-cloud-download-alt"></i>
                    </a>
                </div>
                <table cellpadding="10" class="table table-bordered table-font user-table table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <tr>
                        <th class="t-fields">{{__('text.name')}}</th>
                        <th class="t-fields">{{__('text.surname')}}</th>
                        <th class="t-fields">{{__('text.middleName')}}</th>
                        <th class="t-fields">{{__('text.telephone')}}</th>
                        <th class="t-fields">{{__('text.email')}}</th>
                        <th class="t-fields">{{__('admin.role')}}</th>
                        <th class="t-fields">{{__('actions.edit')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($users)
                        @foreach ($users as $user)
                            <tr>
                                <td class="t-fields-awards">{{$user->name}}</td>
                                <td class="t-fields-awards">{{$user->surname}}</td>
                                <td class="t-fields-awards">{{$user->secondName}}</td>
                                <td class="t-fields-awards">{{$user->studentPhone}}</td>
                                <td class="t-fields-awards">{{$user->email}}</td>
                                <td class="t-fields-awards">{{$user->getRole()}}
                                <td class="t-fields-awards">
                                    <div class="h-action action">
                                        <a href="#" class="btn btn-secondary btn-new-manage" title="{{ __('admin.role') }}"
                                           data-toggle="modal"
                                           data-target="#editRoleModal{{ $user->id }}"><i
                                                class="fas fa-users-cog icon-size"></i></a>
                                        <a href="#" class="btn btn-primary btn-new-manage text-white bg-primary"
                                           data-toggle="modal" title="{{ __('admin.changePassword') }}"
                                           data-target="#editPasswordModal{{ $user->id }}"><i
                                                class="fas fa-edit icon-size"></i></a>
                                    </div>
                                    <div class="h-action action">
                                        <a href="{{ URL::to('/admin/manageUsers/remove/'.$user->id) }}"
                                           class="btn btn-danger btn-new-manage" title="{{ __('actions.delete') }}"><i
                                                class="fas fa-trash-alt icon-size"></i></a>
                                        <a href="{{ URL::to('/admin/manageUsers/viewprofile/'.$user->id) }}"
                                           class="btn btn-info text-white btn-new-manage" title="{{ __('admin.viewProfile') }}">
                                            <i class="fas fa-eye icon-size"></i></a>
                                    </div>
                                    <div class="modal" tabindex="-1" id="editRoleModal{{ $user->id }}"
                                         aria-labelledby="exampleModalLabel{{ $user->id }}" aria-hidden="true"
                                         role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">{{__('admin.changeRole')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="post"
                                                      action="{{URL::to('/admin/manageUsers/changeRole')}}">
                                                    <div class="modal-body">
                                                        @csrf
                                                        <input type="hidden" name="userId" value="{{ $user->id }}">
                                                        <select class="form-control" name="userRole">
                                                            @foreach(Auth::user()->getUserRoles() as $role)
                                                                <option
                                                                    value="{{ $role->id }}">{{ $role->roleName }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('actions.assign')}}</button>
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal" tabindex="-1" id="editPasswordModal{{ $user->id }}"
                                         aria-labelledby="examplePasswordLabel{{ $user->id }}" aria-hidden="true"
                                         role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">{{__('admin.changePassword')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="post"
                                                      action="{{ URL::to('/admin/manageUsers/changePassword' )}}">
                                                    <div class="modal-body">
                                                        @csrf
                                                        <input type="hidden" name="userId" value="{{ $user->id }}">
                                                        <input type="text" class="form-control" name="password">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('actions.edit')}}</button>
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <div class="navigate">
                        {{ $users->links() }}
                    </div>
                </table>
            </div>
        </div>
    </div>
@endsection
