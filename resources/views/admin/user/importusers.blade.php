@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{ __('admin.importUsers') }}</h3></div>
                @include('layouts.messages')
                <form method="post" action="{{ URL::to('/admin/manageUsers/importUsers/import') }}" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="import-background">
                            <i class="fas fa-cloud-upload-alt"></i>
                        </div>
                        <input type="file" name="importFile">
                    </div>
                    <button type="submit" class="btn btn-success">{{ __('actions.import') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
