@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="col-md-12 col-lg-12">
                    <div class="special-card">
                        <div class="card">
                            <div class="card-header bg-primary">
                                <div class="center text-white"><h3>{{__('admin.userCard')}}</h3></div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="profile-img">
                                            <img class="rounded profile-adm-user" src="{{ asset('images/ava.png') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="profile-head">
                                            <h5>{{ $user->surname }} {{ $user->name }} {{ $user->secondName }}</h5>
                                        </div>
                                        <div class="tab-content profile-tab" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home" role="tabpanel"
                                                 aria-labelledby="home-tab">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{ __('admin.facultyName') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <p>{{ $user->getUsersFaculty() }}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{ __('admin.educationalProgramName') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <p>{{ $user->getUsersField() }}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{ __('admin.okr') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <p>{{ $user->getOkr() }}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{ __('admin.group') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <p>{{ $user->group }}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{ __('text.email') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <p>{{ $user->email }}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{ __('text.telephone') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <p>{{ $user->studentPhone }}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{ __('admin.role') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <p>{{ $user->getRole() }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

