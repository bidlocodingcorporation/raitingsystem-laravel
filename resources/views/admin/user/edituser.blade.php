@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('Редагувати користувача')}}</h3></div>
                @include('layouts.messages')
                <form action="{{ URL::to('/admin/manageUsers/store') }}" method="post">
                    <div class="padding-form">
                        <input type="hidden" value="{{$user->id}}" name="user_id">
                        <label for="username">{{__('Ім\'я')}}</label>
                        <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                        <label for="usersurname">{{ __('Прізвище') }}</label>
                        <input type="text" name="surname" class="form-control" value="{{ $user->surname }}">
                        <label for="secondName">{{ __('По-батькові') }}</label>
                        <input type="text" name="surname" class="form-control" value="{{ $user->secondName }}">
                        <label for="secondName">{{ __('Група') }}</label>
                        <input type="text" name="surname" class="form-control" value="{{ $user->secondName }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
