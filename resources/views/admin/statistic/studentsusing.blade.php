@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <table cellpadding="10" class="table table-bordered table-font table-wrapper-scroll-y table-custom-scrollbar student-table-statistic">
                    <thead>
                    <th class="t-fields">{{ __('admin.facultyName') }}</th>
                    <th class="t-fields">{{ __('admin.numberOfUsers') }}</th>
                    <th class="t-fields">{{ __('admin.surnames') }}</th>
                    </thead>
                    <tbody>
                        @foreach($faculties as $faculty)
                            <tr>
                                <td class="t-fields-awards"><p>{{ $faculty->facultyName }}</p></td>
                                <td class="t-fields-awards"><p>{{ $faculty->getSystemUsersValue() }}</p></td>
                                <td class="t-fields-awards">
                                    @foreach($faculty->getSystemUsersSurnames() as $studentSurname)
                                        <p>{{ $studentSurname }}</p>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <div class="navigate">
                        {{ $faculties->links() }}
                    </div>
                </table>
            </div>
        </div>
    </div>
@endsection
