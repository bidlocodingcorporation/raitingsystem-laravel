@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center">
                    <h3>{{ __('admin.theLatestGeneratedDocuments') }}</h3>
                </div>
                <table cellpadding="10" class="table table-bordered table-font table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <th class="t-fields">{{ __('admin.user') }}</th>
                    <th class="t-fields">{{ __('admin.status') }}</th>
                    <th class="t-fields">{{ __('admin.date') }}</th>
                    </thead>
                    <tbody>
                    @foreach($userlogs as $userlog)
                        <tr>
                            <td class="t-fields-awards">{{ $userlog->user['name'].' '.$userlog->user['surname'] }}</td>
                            <td class="t-fields-awards">
                                @if ($userlog->status)
                                    {{ __('admin.documentSent') }}
                                @else
                                    {{ __('admin.documentFailedSent') }}
                                @endif
                            </td>
                            <td class="t-fields-awards">
                                {{ $userlog->created_at }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <div class="navigate">
                        {{ $userlogs->links() }}
                    </div>
                </table>
            </div>
        </div>
    </div>
@endsection
