@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.manageTypeOfPartisipation')}}</h3></div>
                @include('layouts.messages')
                <div class="control-panel">
                    <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#addTypeOfPartisipationModal"
                            title="{{__('admin.addNewTypeOfPartisipation')}}">{{__('actions.add')}}+
                    </button>
                    <button type="button" class="btn btn-danger">{{__('actions.delete')}}</button>
                    <div class="modal fade" id="addTypeOfPartisipationModal" tabindex="-1" role="dialog"
                         aria-labelledby="addTypeOfPartisipationLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="addTypeOfPartisipationLabel">{{__('admin.addNewTypeOfPartisipation')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST"
                                      action="{{URL::to('/admin/manageDocumentTypeOfPartisipation/store')}}">
                                    <div class="modal-body">
                                        <label for="facultyName">{{__('admin.typeOfPartisipationName')}}</label>
                                        <input type="text" class="form-control" name="typeOfPartisipationName" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="btn btn-info" data-toggle="modal" data-target="#importTypeOfPartisipationModal">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </a>
                    <div class="modal fade" id="importTypeOfPartisipationModal" tabindex="-1" role="dialog"
                         aria-labelledby="importTypeOfPartisipationModalLevel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="importTypeOfPartisipationModalLabel">{{__('admin.typeOfPartisipationImport')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageDocumentTypeOfPartisipation/import')}}" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <label for="typeOfPartisipation">{{__('admin.uploadFile')}}</label>
                                        <input type="file" class="form-control" name="importFile" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.import')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="{{ URL::to('/admin/manageDocumentTypeOfPartisipation/export') }}" class="btn btn-info">
                        <i class="fas fa-cloud-download-alt"></i>
                    </a>
                </div>
                <table cellpadding="10" class="table table-levels-awards  table-bordered table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <tr>
                        <th class="t-fields">{{__('actions.highlight')}}</th>
                        <th class="t-fields">{{__('admin.typeOfPartisipationName')}}</th>
                        <th class="t-fields">{{__('actions.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (isset($typesOfPartisipations))
                        @foreach ($typesOfPartisipations as $typesOfPartisipation)
                            <tr>
                                <td class="t-fields-awards"><input type="checkbox"></td>
                                <td class="t-fields-awards">{{$typesOfPartisipation->documentTypeOfPartisipation}}</td>
                                <td class="t-fields-awards">
                                    <div class="h-action action">
                                        <a href="{{URL::to('/admin/manageDocumentTypeOfPartisipation/remove/'.$typesOfPartisipation->id)}}"
                                           class="btn btn-danger btn-new"><i class="fas fa-trash-alt"></i></a>
                                        <a href="#" data-toggle="modal" data-target="#editTypeOfPartisipationModal"
                                           class="btn btn-info btn-new text-white bg-primary"><i
                                                class="fas fa-edit"></i></a>
                                    </div>
                                    <div class="modal fade" id="editTypeOfPartisipationModal" tabindex="-1"
                                         role="dialog" aria-labelledby="editTypeOfPartisipationLabel"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"
                                                        id="editTypeOfPartisipationLabel">{{__('admin.typeOfPartisipationName')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="POST"
                                                      action="{{URL::to('/admin/manageDocumentTypeOfPartisipation/store')}}">
                                                    <div class="modal-body">
                                                        <input type="hidden" value="{{ $typesOfPartisipation->id }}"
                                                               name="typeOfPartisipation_id" class="form-control">
                                                        <label for="facultyName">{{__('admin.typeOfPartisipationName')}}</label>
                                                        <input type="text" class="form-control"
                                                               name="typeOfPartisipationName"
                                                               value="{{ $typesOfPartisipation->documentTypeOfPartisipation }}"
                                                               required>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('actions.save')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <div class="navigate">
                        {{ $typesOfPartisipations->links() }}
                    </div>
                </table>
            </div>
        </div>
    </div>
@endsection
