@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.manageWelcomeInformation')}}</h3></div>
                @include('layouts.messages')
                <a href="#" title="{{ __('admin.importRaitingConfig') }}" class="btn btn-info" data-toggle="modal" data-target="#importRaitingConfigModal">
                    <i class="fas fa-cloud-upload-alt"></i>
                </a>
                <div class="modal fade" id="importRaitingConfigModal" tabindex="-1" role="dialog"
                     aria-labelledby="importRaitingConfigModalLevel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"
                                    id="addFacultyModalLabel">{{__('admin.importRaitingConfig')}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{URL::to('/admin/manageraitingconfig/import')}}" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <label for="importFile">{{ __('admin.uploadFile') }}</label>
                                    <input type="file" class="form-control" name="importFile">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{__('actions.close')}}</button>
                                    <button type="submit" class="btn btn-primary">{{__('actions.import')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <a href="{{ URL::to('/admin/manageraitingconfig/export') }}" title="{{ __('admin.exportRaitingConfig') }}" class="btn btn-info">
                    <i class="fas fa-cloud-download-alt"></i>
                </a>
                <table cellpadding="10"  class="table table-config table-bordered table-font table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <tr>
                        <th class="t-fields">{{ __('admin.responsiblePerson') }}</th>
                        <th class="t-fields">{{ __('admin.E-mailDepartment') }}</th>
                        <th class="t-fields">{{ __('admin.EmailForDocuments') }}</th>
                        <th class="t-fields">{{ __('actions.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($raitingConfig)
                        <tr>
                            <td class="t-fields-awards">{{ $raitingConfig->responsiblePersonName }}</td>
                            <td class="t-fields-awards">{{ $raitingConfig->responsiblePersonEmail }}</td>
                            <td class="t-fields-awards">{{ $raitingConfig->documentsEmail }}</td>
                            <td class="t-fields-awards">
                                <a href="{{ URL::to('/admin/manageraitingconfig/edit') }}"
                                   class="nav-link">{{ __('actions.edit') }}</a>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
