@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                @include('layouts.messages')
                <form method="post" action="{{URL::to('/admin/manageraitingconfig/store')}}">
                    <div class="padding-form">
                        <label for="responsiblePersonName">{{__('admin.responsiblePerson')}}</label>
                        <input type="text" class="form-control" name="responsiblePersonName"
                               value="{{ $raitingConfig->responsiblePersonName }}">
                        <label for="responsiblePersonEmail">{{__('admin.E-mailDepartment')}}</label>
                        <input type="text" class="form-control" name="responsiblePersonEmail"
                               value="{{ $raitingConfig->responsiblePersonEmail }}">
                        <label for="responsiblePersonEmail">{{__('admin.EmailForDocuments')}}</label>
                        <input type="text" class="form-control" name="documentsEmail"
                               value="{{ $raitingConfig->documentsEmail }}">
                        <button type="submit" class="btn btn-primary btn-margin">{{__('actions.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
