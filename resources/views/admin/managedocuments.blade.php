@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.manageDocumentAttributes')}}</h3></div>
                <div class="admin-document-management-navigation">
                    <div class="admin-document-management-navigation-item bg-white shadow-smm">
                        <a href="{{URL::to('/admin/manageDocumentTypeOfAwards')}}">{{__('admin.managementOfTypesOfAwards')}}</a>
                    </div>
                    <div class="admin-document-management-navigation-item bg-white shadow-smm">
                        <a href="{{URL::to('/admin/manageDocumentLevelOfAwards')}}">{{__('admin.managementOfAwardLevels')}}</a>
                    </div>
                    <div class="admin-document-management-navigation-item bg-white shadow-smm">
                        <a href="{{URL::to('/admin/manageDocumentTypeOfPartisipation')}}">{{__('admin.participationTypeManagement')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
