@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.facultyManagement')}}</h3></div>
                @include('layouts.messages')
                <div class="control-panel">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addFacultyModal"
                            title="{{__('admin.addFaculty')}}">{{__('actions.add')}}+
                    </button>
                    <button type="button" class="btn btn-danger">{{__('actions.delete')}}</button>
                    <div class="modal fade" id="addFacultyModal" tabindex="-1" role="dialog"
                         aria-labelledby="addFacultyModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="addFacultyModalLabel">{{__('admin.addFaculty')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageFaculties/save')}}">
                                    <div class="modal-body">
                                        <label for="facultyName">{{__('admin.facultyName')}}</label>
                                        <input type="text" class="form-control" name="facultyName" required>
                                        <label for="facultyEmail">{{__('text.email')}}</label>
                                        <input type="text" class="form-control" name="facultyEmail">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="btn btn-info" data-toggle="modal" data-target="#importFacultyModal">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </a>
                    <div class="modal fade" id="importFacultyModal" tabindex="-1" role="dialog"
                         aria-labelledby="importFacultyModalLevel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="importFacultyModalLabel">{{__('admin.importFaculty')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageFaculties/import')}}" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <label for="typeOfPartisipation">{{__('admin.uploadFile')}}</label>
                                        <input type="file" class="form-control" name="importFile" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.import')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="{{ URL::to('/admin/manageFaculties/export') }}" class="btn btn-info">
                        <i class="fas fa-cloud-download-alt"></i>
                    </a>
                </div>
                <table cellpadding="10" class="table table-faculty table-bordered table-font table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <tr>
                        <th class="t-fields">{{__('actions.highlight')}}</th>
                        <th class="t-fields">{{__('admin.facultyName')}}</th>
                        <th class="t-fields">{{__('text.email')}}</th>
                        <th class="t-fields">{{__('actions.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($factulties)
                        @foreach ($factulties as $factulty)
                            <tr>
                                <td class="t-fields-awards"><input type="checkbox"></td>
                                <td class="t-fields-awards">{{$factulty->facultyName}}</td>
                                <td class="t-fields-awards">{{$factulty->facultyEmail}}</td>
                                <td class="t-fields-awards">
                                    <div class="h-action action">
                                        <a href="{{URL::to('/admin/manageFaculties/remove/'.$factulty->id)}}"
                                           class="btn btn-danger btn-new"><i class="fas fa-trash-alt"></i></a>
                                        <a href="{{URL::to('/admin/manageFaculties/edit/'.$factulty->id)}}"
                                           class="btn btn-info btn-new text-white bg-primary"><i
                                                class="fas fa-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <div class="navigate">
                        {{ $factulties->links() }}
                    </div>
                </table>
            </div>
        </div>
    </div>
@endsection
