@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                @include('layouts.messages')
                <form method="POST" action="{{URL::to('/admin/manageFaculties/save')}}">
                    <div class="padding-form">
                        <input type="hidden" value="{{ $faculty->id }}" name="facultyId">
                        <label for="facultyName">{{__('admin.facultyName')}}</label>
                        <input type="text" class="form-control" name="facultyName" value="{{ $faculty->facultyName }}"
                               required>
                        <label for="facultyEmail">{{__('text.email')}}</label>
                        @if($faculty->facultyEmail)
                            <input type="text" class="form-control" name="facultyEmail"
                                   value="{{ $faculty->facultyEmail }}" maxlength="30">
                        @else
                            <input type="text" class="form-control" name="facultyEmail">
                        @endif
                        <button type="submit" class="btn btn-primary btn-margin">{{__('actions.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
