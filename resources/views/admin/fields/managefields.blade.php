@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.managementOfEducationalPrograms')}}</h3></div>
                @include('layouts.messages')
                <div class="control-panel">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addFieldModal"
                            title="{{__('admin.addNewEducationalProgram')}}">{{__('actions.add')}}+
                    </button>
                    <button type="button" class="btn btn-danger">{{__('actions.close')}}</button>
                    <div class="modal fade" id="addFieldModal" tabindex="-1" role="dialog"
                         aria-labelledby="addFieldModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="addFacultyModalLabel">{{__('admin.addNewEducationalProgram')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageFields/save')}}">
                                    <div class="modal-body">
                                        <label for="fieldName">{{__('admin.educationalProgramName')}}</label>
                                        <input type="text" class="form-control" name="fieldName" required>
                                        <label for="faculty">{{__('admin.facultyName')}}</label>
                                        <select class="form-control" name="faculty">
                                            @foreach ($faculties as $faculty)
                                                <option value="{{ $faculty->id }}">{{ $faculty->facultyName }}</option>
                                            @endforeach
                                        </select>
                                        <label for="okr">{{__('admin.okr')}}</label>
                                        <select class="form-control" name="okr">
                                            @foreach ($okrs as $okr)
                                                <option value="{{ $okr->id }}">{{ $okr->okrLevel }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="btn btn-info" data-toggle="modal" data-target="#importFieldsModal">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </a>
                    <div class="modal fade" id="importFieldsModal" tabindex="-1" role="dialog"
                         aria-labelledby="importFieldsModalLevel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="importFieldsModalLabel">{{__('admin.fieldsImport')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{URL::to('/admin/manageFields/import')}}" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <label for="facultyName">{{__('admin.uploadFile')}}</label>
                                        <input type="file" class="form-control" name="importFile" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('actions.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('actions.import')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="{{ URL::to('/admin/manageFields/export') }}" class="btn btn-info">
                        <i class="fas fa-cloud-download-alt"></i>
                    </a>
                </div>
                <table cellpadding="10" class="table table-bordered table-font table-wrapper-scroll-y horizontal-scroll">
                    <thead>
                    <tr>
                        <th class="t-fields">{{__('actions.highlight')}}</th>
                        <th class="t-fields">{{__('admin.educationalProgramName')}}</th>
                        <th class="t-fields">{{__('admin.facultyName')}}</th>
                        <th class="t-fields">{{__('admin.okr') }}</th>
                        <th class="t-fields">{{__('actions.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($fields)
                        @foreach ($fields as $field)
                            <tr>
                                <td class="t-fields-awards"><input type="checkbox"></td>
                                <td class="t-fields-awards">{{$field->fieldName}}</td>
                                <td class="t-fields-awards">{{$field->getFaculty()}}</td>
                                <td class="t-fields-awards">{{$field->getOkr()}}</td>
                                <td class="t-fields-awards">
                                    <div class="h-action action">
                                        <a href="{{URL::to('/admin/manageFields/remove/'.$field->id)}}"
                                           class="btn btn-danger btn-new"><i class="fas fa-trash-alt"></i></a>
                                        <a href="{{URL::to('/admin/manageFields/edit/'.$field->id) }}"
                                           class="btn btn-info btn-new text-white bg-primary"><i
                                                class="fas fa-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <div class="navigate">
                        {{ $fields->links() }}
                    </div>
                </table>
            </div>
        </div>
    </div>
@endsection
