@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.editEducationalProgram')}}</h3></div>
                @include('layouts.messages')
                <form method="post" class="edit-field" action="{{URL::to('/admin/manageFields/save')}}">
                    <div class="padding-form">
                        <input type="hidden" value="{{$field->id}}" name="field_id">
                        <label for="fieldName">{{__('admin.educationalProgramName')}}</label>
                        <input type="text" class="form-control" name="fieldName" value="{{ $field->fieldName }}"
                               required>
                        <label for="faculty">{{__('admin.facultyName')}}</label>
                        <select class="form-control" name="faculty">
                            @foreach ($faculties as $faculty)
                                <option value="{{ $faculty->id }}">{{ $faculty->facultyName }}</option>
                            @endforeach
                        </select>
                        <label for="okr">{{__('admin.okr')}}</label>
                        <select class="form-control" name="okr">
                            @foreach ($okrs as $okr)
                                <option value="{{ $okr->id }}">{{ $okr->okrLevel }}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary btn-margin">{{__('actions.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
