@extends('layouts.app')
@section('content')
    <div class="container big-container content-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.statistic')}}</h3></div>
                <div class="admin-document-management-navigation">
                    <div class="admin-document-management-navigation-item bg-white shadow-smm">
                        <a href="{{URL::to('/admin/lastActionsStatistic')}}">{{__('admin.lastActivity')}}</a>
                    </div>
                    <div class="admin-document-management-navigation-item bg-white shadow-smm">
                        <a href="{{URL::to('/admin/studentsUsingStatistic')}}">{{__('admin.userUsageStatistic')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
