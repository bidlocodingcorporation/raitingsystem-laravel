@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                <div class="center"><h3>{{__('admin.manageOkr')}}</h3></div>
                @include('layouts.messages')
                <table cellpadding="10" class="table table-bordered table-okr">
                    <thead>
                    <tr>
                        <th class="t-fields">{{__('admin.okr')}}</th>
                        <th class="t-fields">{{__('actions.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <div class="control-panel top-panel">
                        <a href="{{URL::to('/admin/manageOkr/add')}}" class="btn btn-success">{{ __('actions.add') }}</a>
                        <a href="#" title="{{ __('admin.importManageOKR') }}" class="btn btn-info" data-toggle="modal" data-target="#importManageOKRModal">
                            <i class="fas fa-cloud-upload-alt"></i>
                        </a>
                        <div class="modal fade" id="importManageOKRModal" tabindex="-1" role="dialog"
                             aria-labelledby="importManageOKRModalLevel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="addFacultyModalLabel">{{__('admin.importManageOKR')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="POST" action="{{URL::to('/admin/manageOkr/import')}}" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <label for="importFile">{{ __('admin.uploadFile') }}</label>
                                            <input type="file" class="form-control" name="importFile">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">{{__('actions.close')}}</button>
                                            <button type="submit" class="btn btn-primary">{{__('actions.import')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a href="{{ URL::to('/admin/manageOkr/export') }}" title="{{ __('admin.exportManageOKR') }}" class="btn btn-info">
                            <i class="fas fa-cloud-download-alt"></i>
                        </a>
                    </div>
                    @foreach($okrs as $okr)
                        <tr>
                            <td class="t-fields-awards">{{ $okr->okrLevel }}</td>
                            <td class="t-fields-awards">
                                <a href="{{ URL::to('/admin/manageOkr/remove/'.$okr->id) }}"
                                   class="btn btn-danger  btn-new"><i class="fas fa-trash-alt"></i></a>
                                <a href="{{ URL::to('/admin/manageOkr/edit/'.$okr->id) }}"
                                   class="btn btn-info btn-new text-white bg-primary"><i
                                        class="fas fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
