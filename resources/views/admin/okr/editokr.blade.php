@extends('layouts.app')
@section('content')
    <div class="container big-container">
        <div class="row">
            @include('layouts.admin.adminnavigation')
            <div class="col-md-12 col-lg-9 admin-content bg-white shadow-smm">
                @include('layouts.messages')
                <form method="post" action="{{ URL::to('/admin/manageOkr/store') }}">
                    <div class="padding-form">
                        <input type="hidden" value="{{ $okr->id }}" name='okrName_id'>
                        <label for="okrLevel">{{ __('ОКР') }}</label>
                        <input type="text" name="okrLevel" class="form-control" value="{{ $okr->okrLevel }}">
                        <button type="submit" class="btn btn-success btn-margin">{{ __('Змiнити') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
