<?php
$faculties = \App\faculty::all();
$okrs = \App\okr::all();
?>
@extends('layouts.app')
@section('content')
    <div class="container big-container content-container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <form action="{{ URL::to('/student/profile/updateProfile/store') }}" method="post">
                    <div class="card shadow-smm new-card">
                        <div class="card-header bg-secondary new-card-header">
                            <div class="center text-white"><h4>{{__('text.changeProfile')}}</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="button-profile-photo">
                                        <button type="submit"
                                                class="btn btn-success">{{__('actions.save')}}</button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="profile-head">
                                        <div class="tab-content profile-tab" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home" role="tabpanel"
                                                 aria-labelledby="home-tab">

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{__('text.surname')}}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group input-group-sm">
                                                            <input type="text" class="form-control"
                                                                   value="{{ Auth::user()->surname }}"
                                                                   name="surname">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{__('text.name')}}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group input-group-sm">
                                                            <input type="text" class="form-control"
                                                                   value="{{ Auth::user()->name }}" name="name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>{{__('text.middleName')}}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group input-group-sm">
                                                            <input type="text" class="form-control"
                                                                   value="{{ Auth::user()->secondName }}"
                                                                   name="secondName">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content profile-tab" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel"
                                             aria-labelledby="home-tab">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>{{ __('text.faculty') }}</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group input-group-sm">
                                                        <select id="faculty_id"
                                                                class="form-control @error('faculty_id') is-invalid @enderror"
                                                                name="faculty_id"
                                                                onchange="dropdown(this.value);"
                                                                required>
                                                            @foreach ($faculties as $faculty)
                                                                <option
                                                                    value="{{ $faculty->id }}">{{ $faculty->facultyName }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>{{ __('text.educationalProgram') }}</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group input-group-sm">
                                                        <select id="field_id"
                                                                class="form-control @error('field') is-invalid @enderror"
                                                                name="field_id" required></select>
                                                        {{--@error('field')--}}
                                                        {{--<span class="invalid-feedback" role="alert">--}}
                                                        {{--<strong>{{ $message }}</strong>--}}
                                                        {{--</span>--}}
                                                        {{--@enderror--}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>{{ __('text.okr') }}</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group input-group-sm">
                                                        <select id="okr_id"
                                                                class="form-control @error('okr') is-invalid @enderror"
                                                                name="okr_id" required>
                                                            @foreach($okrs as $okr)
                                                                <option
                                                                    value="{{ $okr->id }}">{{ $okr->okrLevel }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>{{ __('text.group') }}</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group input-group-sm">
                                                        <input type="text" class="form-control"
                                                               value="{{ Auth::user()->group }}" name="group">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>{{ __('text.telephone') }}</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group input-group-sm">
                                                        <input type="text" class="form-control"
                                                               value="{{ Auth::user()->studentPhone }}"
                                                               name="studentPhone">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            var state = $('#faculty_id').val();
            dropdown(state);
        });

        function dropdown(msg) {
            var state = msg;
            $.ajax({
                url: '/filterByFaculty/' + state,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $('#field_id').empty();
                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].id;
                            var fieldName = response['data'][i].fieldName;
                            var option = "<option value='" + id + "'>" + fieldName + "</option>";

                            $("#field_id").append(option);
                        }
                    }
                }
            });
        }
    </script>
@endsection
