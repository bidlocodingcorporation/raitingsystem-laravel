@extends('layouts.app')

@section('content')
    <div class="container big-container ">
        <div class="row">
            <div class="col-md-11 col-lg-11 user-content bg-white shadow-smm radius">
                <div class="add-document-form-fields">
                    @include('layouts.messages')
                    @if ($helper->getTotalMarks() < $helper::POINTS_LIMIT)
                        <div class="add-document-btn">
                            <button type="button" data-toggle="modal" data-target="#addDocumentModal"
                                    class="btn btn-success">{{__('actions.addDocument')}}</button>
                            <div class="modal fade" id="addDocumentModal" tabindex="-1" role="dialog"
                                 aria-labelledby="addDocumentLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"
                                                id="addDocumentModalLabel">{{__('actions.addDocument')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{URL::to('/student/createDocument/store')}}"
                                              enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <label for="typeOfAward">{{__('admin.typeOfAward')}}</label>
                                                <select class="form-control" name="typeOfAward" id="typeOfAward"
                                                        required onchange="setProtocolField(this.value)">
                                                    @foreach ($documentTypesOfAwards as $documentTypeOfAward)
                                                        <option
                                                            value="{{ $documentTypeOfAward->id }}">{{ $documentTypeOfAward->documentAwardType }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="levelOfAward">{{__('admin.levelOfAward')}}</label>
                                                <select class="form-control" name="levelOfAward" id="levelOfAward" required>
                                                    @foreach($documentLevelOfAwards as $documentLevelOfAward)
                                                        <option value="{{ $documentLevelOfAward->id }}">{{ $documentLevelOfAward->documentLevelOfAward }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="typeOfPartisipation">{{__('admin.typeOfPartisipation')}}</label>
                                                <select class="form-control" name="typeOfPartisipation" required>
                                                    @foreach ($documentTypesOfPartisipations as $documentTypeOfPartisipation)
                                                        <option
                                                            value="{{ $documentTypeOfPartisipation->id }}">{{ $documentTypeOfPartisipation->documentTypeOfPartisipation }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="qty">{{ __('admin.qty') }}</label>
                                                <input type="text" name="qty" class="form-control" id="qty">
                                                <label for="documentImage">{{__('admin.photoOfTheDocument')}}</label>
                                                <input type="file" name="documentImages[]" id="documentImage"
                                                       class="form-control">
                                                <a class="btn btn-success" onclick="addImageField()"><i class="fas fa-plus-square"></i>{{ __('admin.addMoreImages') }}</a>
                                                <div class="form-group">
                                                    <a href="#" data-toggle="tooltip" title="{{ __('Зверніться за контактною поштою '.$config->responsiblePersonEmail) }}">{{ __('text.haveAQuestion') }}</a>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">{{__('actions.close')}}</button>
                                                <button type="submit"
                                                        class="btn btn-primary">{{__('actions.save')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="students-documents-list">
                    @if (count($documents) > 0)
                        <table cellpadding="10" class="table-wrapper-scroll-y table user-table-document table-bordered table-font table-custom-scrollbar">
                            <thead>
                            <tr>
                                <th class="t-fields">{{__('admin.qty')}}</th>
                                <th class="t-fields">{{__('admin.typeOfAward')}}</th>
                                <th class="t-fields">{{__('admin.levelOfAward')}}</th>
                                <th class="t-fields">{{__('admin.typeOfPartisipation')}}</th>
                                <th class="t-fields">{{__('admin.photoOfTheDocument')}}</th>
                                <th class="t-fields">{{ __('admin.protocol') }}</th>
                                <th class="t-fields">{{ __('admin.numberOfPoints') }}</th>
                                <th class="t-fields">{{ __('actions.action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($documents as $document)
                                <tr>
                                    <td class="t-fields-awards">{{ $document->qty }}</td>
                                    <td class="t-fields-awards">{{ $document->getDocumentLevelOfAwards() }}</td>
                                    <td class="t-fields-awards">{{ $document->getDocumentTypeOfAwards() }}</td>
                                    <td class="t-fields-awards">{{ $document->getTypeOfPartisipation() }}</td>
                                    <td class="t-fields-awards">
                                        <div class="image-box">
                                            @foreach($document->getImages() as $documentImage)
                                                <div class="border border-dark rounded">
                                                    <img class="image-size fill items"
                                                         src="{{ asset('/storage/'.$documentImage->documentImage) }}"
                                                         onclick="window.open(this.src)">
                                                </div>
                                            @endforeach
                                        </div>
                                    </td>
                                    @if($document->protocol)
                                        <td class="t-fields-awards">
                                            <div class="image-box2">
                                                <div class="border border-dark rounded">
                                                    <img class="image-size fill items"
                                                         src="{{ asset('/storage/'.$document->protocol) }}"
                                                         onclick="window.open(this.src)">
                                                </div>
                                            </div>
                                        </td>
                                    @else
                                        <td class="t-fields-awards">-</td>
                                    @endif
                                    <td class="document-mark t-fields-awards">{{ $document->getDocumentMark() }}</td>
                                    <td class="t-fields-awards">
                                        <a href="{{ URL::to('/student/createDocument/remove/'.$document->id) }}"
                                           title="{{__('Видалити')}}">
                                            <button class="btn btn-danger document-remove-btn"><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td class="t-fields-awards"></td>
                                <td class="t-fields-awards"></td>
                                <td class="t-fields-awards"></td>
                                <td class="t-fields-awards"></td>
                                <td class="t-fields-awards"></td>
                                <td class="t-fields-awards"></td>
                                <td class="t-fields-awards"><b>{{ __('admin.inGeneral') }}</b>{{ $helper->getTotalMarks() }}
                                </td>
                                <td class="t-fields-awards"></td>
                            </tr>
                            </tfoot>
                        </table>
                        @if ($helper->getTotalMarks() >= $helper::POINTS_LIMIT)
                            <div class="alert alert-danger" role="alert">
                                <span>{{ __('text.totalPointsOutput') }}</span>
                            </div>
                        @endif
                    @endif
                    @if(count($documents) > 0)
                        <form method="get" action="{{ URL::to('/student/createDocument/generatePDF') }}">
                             <div class="select-inputs">
                                 <div class="select-input-cource">
                                     <label for="studyCource">{{__('text.course')}}</label>
                                     <select class="form-control" name="studyCource">
                                         <option>1</option>
                                         <option>2</option>
                                         <option>3</option>
                                         <option>4</option>
                                     </select>
                                 </div>
                                 <div class="select-input-semestr">
                                     <label for="studySemestr">{{__('text.semester')}}</label>
                                     <select class="form-control" name="studySemestr">
                                         <option>1</option>
                                         <option>2</option>
                                     </select>
                                 </div>
                                 @if (count($documents) > 0)
                                     <div class="generate-pdf-button">
                                         <button class="btn btn-dark" id="btnSubmit" onclick="alertMessageForUser()" type="submit">
                                             {{ __('actions.createDocument') }}
                                         </button>
                                     </div>
                                 @endif
                             </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    /*document.addEventListener("DOMContentLoaded", function (event) {
        var state = $('#typeOfAward').val();
        getLevelOfAward(state);
    });
*/
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    function alertMessageForUser() {
        alert('{{ __('text.documentGenerationTimeMessage')  }}');
        hideSubmit();
    }

    function getLevelOfAward(msg) {
        var state = msg;
        $.ajax({
            url: '/getTypeOfAwardsByLevel/' + state,
            type: 'get',
            dataType: 'json',
            success: function (response) {
                setProtocolField(state);
                $('#levelOfAward').empty();
                var len = 0;
                if (response['data']) {
                    len = response['data'].length;
                }

                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var id = response['data'][i].id;
                        var documentLevelOfAward = response['data'][i].documentLevelOfAward;
                        var option = "<option value='" + id + "'>" + documentLevelOfAward + "</option>";

                        $("#levelOfAward").append(option);
                    }
                }
            }
        });
    }

    function setProtocolField(id) {
        if (id === '6') {
            if ($('#protocol').length === 0) {
                $('#documentImage').after('<label>Витяг</label><input type="file" class="form-control" name="protocol" id="protocol">');
            }
        }
    }

    function addImageField() {
        $('#documentImage').after('<label for="documentImage">Фото документа</label><input type="file" name="documentImages[]" class="form-control">');
    }

    function imgWindow() {
        window.open("image");
    }

    function hideSubmit() {
        $('#btnSubmit').hide();
    }
</script>
