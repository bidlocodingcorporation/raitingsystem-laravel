<!DOCTYPE html>
<html lang="RU">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Документ</title>
    <style>
        body {
            font-family: "DejaVu Sans", serif;
            page-break-after: always;
        }

        .student-document-head {
            font-size: 12px;
            margin-left: 350px;
        }

        .student-document-type {
            margin-top: 14px;
            font-size: 14px;
            text-transform: uppercase;
            text-align: center;
        }

        .student-document-body {
            margin-top: 14px;
            margin-left: 20px;
            font-size: 16px;
            text-indent: 38px;
        }

        table {
            margin-top: 14px;
            margin-left: 14px;
        }

        tr {
            border-bottom: 1px black;
        }

        .date, .autograph {
            display: inline-block;
        }

        .date {
            margin-top: 20px;
            margin-left: 20px;
            font-size: 14px;
        }

        .autograph {
            margin-left: 150px;
            font-size: 14px;
        }

        .additional-documents {
            margin-top: 38px;
            margin-left: 20px;
        }

        .stedpendial-compission {
            margin-top: 38px;
            margin-left: 20px;
        }

        .compission-members {
            margin-top: 38px;
            margin-left: 20px;
        }

    </style>
</head>
<body>
    <div class="students-documents-list">
        <div class="student-document-head">
            <p>
                Ректору Запорiзького нацiонального унiверситету<br>
                Фролову М.О.<br> студента(ки): {{ $kurs }} курсу<br>
                ОКР(ОР): {{ Auth::user()->getOkr() }}<br>
                факультету: {{ Auth::user()->getUsersFaculty() }}<br>
                напрям пiдготовки: {{ Auth::user()->getUsersField() }}<br>
                група: {{ Auth::user()->group }}<br>
                Форма навчання: державне замовлення<br>
                № моб.т. {{ Auth::user()->studentPhone }}<br>
                {{ Auth::user()->surname.' '.Auth::user()->name.' '.Auth::user()->secondName}}
            </p>
        </div>
        <div class="student-document-type">
            <span><b>Заява</b></span>
        </div>
        <div class="student-document-body">
            Прошу зарахувати до рейтингу академiчної успiшностi бали за досягнення в позанавчальнiй дiяльностi в {{ $semestr }} семестрi {{ $helper->getPastYear() }} - {{ date('Y') }} року.
        </div>
        <table>
            <thead>
                <tr>
                    <th><strong>{{ __('Кiлькiсть') }}</strong></th>
                    <th><strong>{{ __('Тип нагороди') }}</strong></th>
                    <th><strong>{{ __('Рiвень нагороди') }}</strong></th>
                    <th><strong>{{ __('Тип участi') }}</strong></th>
                    <th><strong>{{ __('Кiлькiсть балiв') }}</strong></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($documents as $document)
                    <tr>
                        <td>{{ $document->qty }}</td>
                        <td>{{ $document->getDocumentLevelOfAwards() }}</td>
                        <td>{{ $document->getDocumentTypeOfAwards() }}</td>
                        <td>{{ $document->getTypeOfPartisipation() }}</td>
                        <td class="document-mark">{{ $document->getDocumentMark() }}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        <strong>Загалом: </strong>{{ $helper->getTotalMarks() }} балiв
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
        <div class="date">
            <span>________</span>
            <div>Дата</div>
        </div>
        <div class="autograph">
            <span>_______________</span>
            <div>Пiдпис студента</div>
        </div>
        <div class="additional-documents">
            <span>До заяви додаються завiренi ксерокопії документiв, якi пiдтверджують досягнення</span>
        </div>
        <div class="stedpendial-compission">
            <span>Голова стипендіальної комісії факультету____________________________________</span>
        </div>
        <div class="compission-members">
            <span>Члени стипендіальної комісії факультету:</span>
            <div>__________________________</div>
            <div>__________________________</div>
            <div>__________________________</div>
            <div>__________________________</div>
        </div>
    </div>
</body>
</html>
