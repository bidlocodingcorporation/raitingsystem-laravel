@extends('layouts.app')
@section('content')
    <div class="container big-container content-container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card shadow-smm new-card">
                    <div class="card-header bg-secondary new-card-header">
                        <div class="center text-white"><h3>{{__('text.myProfile')}}</h3></div>
                    </div>
                    <div class="card-body">
                        @include('layouts.messages')
                        <div class="row">
                            <div class="col-md-4">
                                <div class="button-profile-photo">
                                    <a class="btn btn-secondary text-white"
                                       href="{{ URL::to('/student/profile/updateProfile') }}">{{__('actions.edit')}}</a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="profile-head">
                                    <h5>{{ Auth::user()->surname }} {{ Auth::user()->name }} {{ Auth::user()->secondName }}</h5>
                                </div>
                                <div class="tab-content profile-tab" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>{{ __('text.faculty') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{ Auth::user()->getUsersFaculty() }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>{{ __('text.educationalProgram') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{ Auth::user()->getUsersField() }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>{{ __('text.okr') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{ Auth::user()->getOkr() }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>{{ __('text.group') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{ Auth::user()->group }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>{{ __('text.email') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{ Auth::user()->email }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>{{ __('text.telephone') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{ Auth::user()->studentPhone }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

