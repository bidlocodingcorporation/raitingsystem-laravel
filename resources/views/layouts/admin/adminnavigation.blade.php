<div class="col-md-12 col-lg-2 sidebar">
    <div class="mini-submenu shadow-smm sidebar-radius">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </div>
    <div class="list-group shadow-smm sidebar-radius">
        <span href="#" class="list-group-main text-white bg-secondary">
            <p>{{__('admin.menuNavigation')}}
            <span class="pull-right" id="slide-submenu">
                <i class="fa fa-times"></i>
            </span>
            </p>

        </span>
        <ul class="list-group-net">
            <li class="list-group-item"><a href="{{URL::to('/admin')}}">{{__('admin.controlPanel')}}</a>
            <li class="list-group-item"><a href="{{URL::to('/admin/manageDocuments')}}">{{__('admin.manageDocumentAttributes')}}</a></li>
            <li class="list-group-item"><a href="{{URL::to('/admin/manageFields')}}">{{__('admin.managementOfEducationalPrograms')}}</a></li>
            <li class="list-group-item"><a href="{{URL::to('/admin/manageFaculties')}}">{{__('admin.facultyManagement')}}</a></li>
            <li class="list-group-item"><a href="{{URL::to('/admin/managePointsCalc')}}">{{__('admin.scoring')}}</a></li>
            <li class="list-group-item"><a href="{{URL::to('/admin/manageOkr')}}">{{ __('admin.manageOkr') }}</a></li>
            <li class="list-group-item"><a href="{{URL::to('/admin/manageUsers')}}">{{__('admin.userManagement')}}</a></li>
            <li class="list-group-item"><a href="{{URL::to('/admin/manageraitingconfig')}}">{{__('admin.manageWelcomeInformation')}}</a></li>
        </ul>
    </div>
</div>
<script>
    if($(window).width() < 991) {
        $(function(){
            $('.list-group').hide();
            $('.mini-submenu').fadeIn();

            $('#slide-submenu').on('click',function() {
                $(this).closest('.list-group').fadeOut('slide',function(){
                    $('.mini-submenu').fadeIn();
                });
            });

            $('.mini-submenu').on('click',function(){
                $(this).next('.list-group').toggle('slide');
                $('.mini-submenu').hide();
            })
        });
    } else {
        $('#slide-submenu').hide();
    }
</script>
