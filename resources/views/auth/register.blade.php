<?php
$faculties = \App\faculty::all();
$okrs = \App\okr::all();
?>

@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Зробити облiковий запис') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('text.surname') }}</label>
                            <div class="col-md-6">
                                <input id="surname" placeholder="{{ __('text.surname') }}" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required>
                                @error('surname')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('text.name') }}</label>
                            <div class="col-md-6">
                                <input id="name" placeholder="{{ __('text.name') }}" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('text.middleName') }}</label>
                            <div class="col-md-6">
                                <input id="secondName" placeholder="{{ __('text.middleName') }}" type="text" class="form-control @error('secondName') is-invalid @enderror" name="secondName" value="{{ old('secondName') }}" required>
                                @error('secondName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="faculty_id" class="col-md-4 col-form-label text-md-right">{{ __('text.faculty') }}</label>
                            <div class="col-md-6">
                                <select id="faculty_id" class="form-control @error('faculty_id') is-invalid @enderror" name="faculty" onchange="dropdown(this.value);" required>
                                    <option></option>
                                    @foreach ($faculties as $faculty)
                                        <option value="{{ $faculty->id }}">{{ $faculty->facultyName }}</option>
                                    @endforeach
                                </select>
                                @error('faculty')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="field_id" class="col-md-4 col-form-label text-md-right">{{ __('text.educationalProgram') }}</label>
                            <div class="col-md-6">
                                <select id="field_id" class="form-control @error('field') is-invalid @enderror" name="field" required></select>
                                @error('field')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="okr_id" class="col-md-4 col-form-label text-md-right">{{ __('text.okr') }}</label>
                            <div class="col-md-6">
                                <select id="okr_id" class="form-control @error('okr') is-invalid @enderror" name="okr" required>
                                    @foreach($okrs as $okr)
                                        <option value="{{ $okr->id }}">{{ $okr->okrLevel }}</option>
                                    @endforeach
                                </select>
                                @error('okr')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="group" class="col-md-4 col-form-label text-md-right">{{ __('text.group') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ __('text.group') }}" name="group" maxlength="15" class="form-control @error('group') is-invalid @enderror" required>
                                @error('group')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('text.password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('text.confirmPassword') }}</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="user-email" class="col-md-4 col-form-label text-md-right">{{ __('text.email') }}</label>
                            <div class="col-md-6">
                                <input type="email" placeholder="{{ __('text.email') }}" class="form-control @error('email') is-invalid @enderror" name="email" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="student-phone" class="col-md-4 col-form-label text-md-right">{{ __('text.telephone') }}</label>
                            <div class="col-md-6">
                                <input type="tel" placeholder="{{ __('text.telephone') }}" class="form-control @error('studentPhone') is-invalid @enderror" name="studentPhone" required>
                                @error('studentPhone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('actions.create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        var state = $('#faculty_id').val();
        dropdown(state);
    });

    function dropdown(msg) {
        var state = msg;
        $.ajax({
            url: '/filterByFaculty/'+state,
            type: 'get',
            dataType: 'json',
            success: function(response){
                $('#field_id').empty();
                var len = 0;
                if (response['data'] != null) {
                    len = response['data'].length;
                }

                if (len > 0) {
                    for (var i=0;i<len;i++) {
                        var id = response['data'][i].id;
                        var fieldName = response['data'][i].fieldName;
                        var option = "<option value='"+id+"'>"+fieldName+"</option>";

                        $("#field_id").append(option);
                    }
                }
            }
        });
    }
</script>
