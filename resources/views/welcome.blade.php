@extends('layouts.app')

@section('content')
    <div class="container content-container">
        <div class="row d-flex justify-content-center bg-white shadow-smm radius">
            <div class="col-lg-8 col-xl-7 radius-container">
                <div class="welcome-content">
                    <h1 class="welcome-content-header">{{__('welcome.welcome')}}</h1>
                    <div class="welcome-content-text">
                        <span>{{ __('welcome.toCreateADocument') }}</span>
                        <ul class="algorithm">
                            <li class="algorithm-item">{{ __('welcome.createAnAccount') }}</li>
                            <li class="algorithm-item">{{ __('welcome.addInformationAboutYourAchievements') }}</li>
                            <li class="algorithm-item">{{ __('welcome.clickTheGenerateDocumentButton') }}</li>
                            <li class="algorithm-item">{{ __('welcome.clickTheDownloadButton') }}</li>
                        </ul>
                    </div>
                    <div class="welcome-feedback">
                        <p class="mb-1"><b>{{__('welcome.feedback')}}</b></p>
                        <div class="ml-4">
                            <p>{{ $raitingConfig->responsiblePersonName }}<br>
                            {{ $raitingConfig->responsiblePersonEmail }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
