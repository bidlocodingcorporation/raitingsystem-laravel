<?php
    return [
        'controlPanel' => 'Панель керування',
        'statistic' => 'Статистика',
        'lastActivity' => 'Остання активність на сайті',
        'userUsageStatistic' => 'Статистика використання студентами',
        'manageDocumentAttributes' => 'Керування атрибутами документiв',
        'managementOfTypesOfAwards' => 'Керування типами нагород',
        'addTypeOfAward' => 'Додати тип нагороди',
        'managementOfAwardLevels' => 'Керування рiвнями нагород',
        'importTypeOfAward' => 'Імпорт типу нагород',
        'uploadFile' => 'Завантажте файл',
        'importTypeOfAwardsSuccess' => 'Тип нагород успішно імпортовано',
        'participationTypeManagement' => 'Керування типами участi',
        'nameOfTheTypeOfParticipation' => 'Назва типу участi',
        'typeOfAward' => 'Тип нагороди',
        'levelOfAward' => 'Рівень нагороди',
        'addTypeOfAwards' => 'Додати тип нагород',
        'nameOfTheTypeOfAwards' => 'Назва типу нагород',
        'typeOfPartisipation' => 'Тип участі',
        'qty' => 'Кількість',
        'editTheTypeOfAward' => 'Редагувати тип нагороди',
        'addANewLevelOfRewards' => 'Додати новий рiвень нагород',
        'addLevelOfRewards' => 'Додати рівень нагороди',
        'nameOfTheLevelOfAwards' => 'Назва рiвня нагород',
        'importLevelOfAwards' => 'Імпортувати рівні нагород',
        'exportLevelOfAwards' => 'Експортувати рівні нагород',
        'manageTypeOfPartisipation' => 'Керування типом участi',
        'addNewTypeOfPartisipation' => 'Додати тип участi',
        'typeOfPartisipationName' => 'Назва типу участі',
        'typeOfPartisipationImport' => 'Імпортувати типи участі',
        'facultyManagement' => 'Керування факультетами',
        'addFaculty' => 'Додати факультет',
        'facultyName' => 'Назва факультету',
        'importFaculty' => 'Імпортувати факультети',
        'managementOfEducationalPrograms' => 'Керування освiтнiми програмами',
        'addNewEducationalProgram' => 'Додати нову спецiальнiсть',
        'educationalProgramName' => 'Назва освітньої програми',
        'fieldsImport' => 'Імпортувати освітні програми',
        'okr' => 'ОКР',
        'importManageOKR' => 'Імпорт OKR',
        'exportManageOKR' => 'Експорт OKR',
        'editEducationalProgram' => 'Редагувати освiтню програму',
        'manageOkr' => 'Керування ОКР',
        'scoring' => 'Керування пiдрахунком балiв',
        'pointsForIndividualParticipation' => 'Бали за одноосiбну участь',
        'pointsForCollectiveParticipation' => 'Бали за колективну участь',
        'importPointsCalc' => 'Імпортувати бали',
        'manageWelcomeInformation' => 'Керування привiтальною iнформацiю',
        'importRaitingConfig' => 'Імпорт привiтальної iнформацiї',
        'exportRaitingConfig' => 'Експорт привiтальної iнформацiї',
        'responsiblePerson' => 'Відповідальна особа',
        'E-mailDepartment' => 'Електрона пошта відділу',
        'EmailForDocuments' => 'Електрона пошта для документів',
        'theLatestGeneratedDocuments' => 'Останні сформовані документи',
        'user' => 'Користувач',
        'status' => 'Статус дії',
        'date' => 'Дата',
        'documentSent' => 'Документ відправлено',
        'documentFailedSent' => 'Документ не відправлено',
        'userManagement' => 'Керування користувачами',
        'changePassword' => 'Зміна пароля',
        'numberOfUsers' => 'Кількість користувачів',
        'surnames' => 'Прізвища',
        'userCard' => 'Картка користувача',
        'viewProfile' => 'Перегляд профіля',
        'group' => 'Група',
        'role' => 'Роль',
        'changeRole' => 'Змiнити роль',
        'photoOfTheDocument' => 'Фото документа',
        'addMoreImages' => 'Додати ще зображення',
        'protocol' => 'Витяг',
        'numberOfPoints' => 'Кiлькiсть балiв',
        'inGeneral' => 'Загалом: ',
        'menuNavigation' => 'Навігаційне меню',
        'importUsers' => 'Імпорт користувачів',
        'exportUsers' => 'Експорт користувачів',
        'importSuccess' => 'Користувачів успішно імпортовано',
        'exportSuccess' => 'Користувачів успішно експортовано'
    ];
