<?php
    return array(
        'welcome' => 'Ласкаво просимо!',
        'toCreateADocument' => 'Для того щоб створити документ:',
        'createAnAccount' => 'Створiть облiковий запис',
        'addInformationAboutYourAchievements' => 'Додайте iнформацiю про вашi досягнення',
        'clickTheGenerateDocumentButton' => 'Натиснiть кнопку "Сформувати документ"',
        'clickTheDownloadButton' => 'Натиснiть кнопку "Завантажити"',
        'feedback' => 'Зворотнiй зв\'язок:'
    );
