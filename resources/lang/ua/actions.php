<?php
    return [
      'highlight' => 'Видiлити',
      'action' => 'Дія',
      'add' => 'Додати ',
      'edit' => 'Редагувати',
      'delete' => 'Видалити',
      'close' => 'Закрити',
      'save' => 'Зберегти',
      'assign' => 'Призначити',
      'create' => 'Створити',
      'addDocument' => 'Додати документ',
      'createDocument' => 'Створити документ',
      'import' => 'Імпортувати',
      'importSuccessMsg' => 'Імпорт успішно виконано',
      'export' => 'Експортувати'
    ];
