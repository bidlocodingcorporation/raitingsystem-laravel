<?php
    return array(
        'email' => ':attribute повинен бути дійсною адресою електронної пошти.',
        'min' => [
            'numeric' => 'The :attribute must be at least :min.',
            'file' => 'The :attribute must be at least :min kilobytes.',
            'string' => ':attribute має сладатися мінімум із :min символів.',
            'array' => 'The :attribute must have at least :min items.',
        ],
    );
