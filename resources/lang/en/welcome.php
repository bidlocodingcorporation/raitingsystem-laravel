<?php
    return array(
      'welcome' => 'Welcome!',
      'toCreateADocument' => 'To create a document:',
      'createAnAccount' => 'Create an account',
      'addInformationAboutYourAchievements' => 'Add information about your achievements',
      'clickTheGenerateDocumentButton' => 'Click the "Generate Document" button',
      'clickTheDownloadButton' => 'Click the "Download" button',
      'feedback' => 'Feedback:'
    );
