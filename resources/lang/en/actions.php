<?php
    return [
      'highlight' => 'Highlight',
      'action' => 'Action',
      'add' => 'Add ',
      'edit' => 'Edit',
      'delete' => 'Delete',
      'close' => 'Close',
      'save' => 'Save',
      'assign' => 'Assign',
      'create' => 'Create',
      'addDocument' => 'Add a document',
      'createDocument' => 'Create document',
      'import' => 'Import',
      'export' => 'Export',
      'importSuccessMsg' => 'Import successful',

    ];
